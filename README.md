# Experiments on parameterized algorithms for graph separation problems: half-integral relaxations and matroid-based kernelization

Souce code repository with tests and results.

## Prerequisites

For compilation purposes we use CMake system, you will need to install it in order to compile.
Definetely works under versions >= 3.7.2, but should also work under older ones.
Kernelization algorithm require GNU Multiprecision Arithmetic (GMP).

## Compilation

To compile solvers for OCT, MWC, and FVS, use CMake in the main directory:

```
mkdir build
cd build/
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

To compile kernelization algorithms, type `make oct` and `make mwc`
in the `kernelization/` subdirectory.


## Usage of the solvers

### Odd Cycle Transversal solver

`./build/oct <sol_type>`

#### Possible parameters

* CSP - default, plain CSP 0/1/all solver
* CSPLB1 - CSP 0/1/all solver with the standard lower bound prunning
* CSPLB2 - CSP 0/1/all solver with the more tight lower bound prunning
* CSPred - pipelined CSP solver with a separate preprocessing step

#### Input and output

The first line of an input text file consists of two integers, the number of vertices
and the number of edges. Each edge is described later in a separate line, consisting the indices
of its endpoints (starting from 0).

The output consists of two lines. The first line contains the size of the found solution, where the second line
contains the list of the vertices in the found solution.

### Multiway Cut solver

`./build/mwc <sol_type>`

#### Possible parameters

* CSP - default, plain CSP 0/1/all solver
* CSPLB1 - CSP 0/1/all solver with the standard lower bound prunning
* CSPLB2 - CSP 0/1/all solver with the more tight lower bound prunning
* CSPred - pipelined CSP solver with a separate preprocessing step
* ImpSeps - classic algorithm based on important separators
* check - a second implementation of ImpSeps, more crude and not optimized (implemented early in the project to check correctness)
* appx - approximation algorithm

The options CSP, CSPLB1, CSPLB2, CSPred, and ImpSeps can be followed by a `CC` option that
enables splitting into connected components if the graph becomes disconnected.

#### Input and output

The first line of an input text file consists of three integers, the number of vertices,
the number of edges, and the number of terminals. The second line lists terminals (0-indexed).
The remaining lines describe edges.
Each edge is described later in a separate line, consisting the indices of its endpoints
(starting from 0).

The output consists of two lines. The first line contains the size of the found solution, where the second line contains the list of the vertices in the found solution.

### Feedback Vertex Set solver

For testing, we also implemented a front-end for Feedback Vertex Set and added it to previous FVS solver
by Kiljan and Pilipczuk from https://bitbucket.org/marcin_pilipczuk/fvs-experiments. To run the FVS solver type

`./build/fvs <sol_type>`

#### Possible parameters

* 8 - Default (if no parameter), Runs Cao solution
* 5 - Iterative Compression solution
* 3.62 - Kociumaka Pilipczuk 
* PD - Cao 8^k solution, branching first on vertices with biggest deg including double neighbours
* PU - Cao 8^k, branching first on vertices with maximum number of neighbours in Undeletable set
* Full - Runs Cao solution with all possible optimizations
* LP - branching based on Iwata paper
* Appx - Runs approximate solution
* CSP - CSP 0/1/All solver

#### Possible additional parameters

* noCC - doesn't use breaking Graph into Connected components reduction
* noDeg3 - Doesn't use deg3 solver in solution
* deg3 - Uses deg3 solver in solution (parameter only for LP branching)
* IC - Uses iterative compression routine while branching (avaialable for 8, PD, PU)
* Full - uses all of the Optimizations available for chosen solution

#### Input and Output

Program takes as an input list of edges (as pairs u v, separated by a whitespace), one pair per row. Vertices names can be any string, with only exception they can't start from # - it is used for commented out lines. 
The format is borrowed from PACE 2016. (To find out more look onto website https://pacechallenge.wordpress.com/pace-2016/track-b-feedback-vertex-set/)

Example Input:

```
a b
b c
c d
d a
```

It corresponds to a cycle graph made of 4 vertices.

On the standard output the solution returns two lines:
- In the first line a single integer N denoting size of the solution found
- In the second line N strings corresponding to the names of vertices that were put in solution

Example output (for the Input above):
```
1
c
```

## Usage: kernelization

`./kernelization/oct < test`
and
`./kernelization/mwc < test`

The input formats are as in the previous section. The algorithm outputs the reduced instance
on stdout and outputs report on stderr.

## Authors

* **Marcin Pilipczuk**
* **Michal Ziobro**


We hereby invite you to visit our project site: http://kernelization-experiments.mimuw.edu.pl/

This repository contains parts of the code by **Krzysztof Kiljan** and Marcin Pilipczuk for Feedback Vertex Set from https://bitbucket.org/marcin_pilipczuk/fvs-experiments

This code is licensed under simplified BSD license (see the LICENSE file).
