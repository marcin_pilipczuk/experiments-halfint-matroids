#include <iostream>

#include "basic.h"
#include "graph.h"

std::pair<Graph,int> reduceNeighborhoods(std::pair<Graph,int> input,VertexSet const& terminals) {
	auto [graph,opt] = input;
	
	for (int t : terminals) for (int t1 : terminals)
		if (t1 != t)
			for (int v : graph[t]) for (int w : graph[t1])
				if (v == w)
					return reduceNeighborhoods(mp(removeVertex(move(graph),v),opt-1),terminals);

	return input;
}

int main() {
	int n,m,t;
	std::cin >> n >> m >> t;

	VertexSet terminals;

	for (int i = 1; i <= t; i++) {
		int tmp_t;
		std::cin >> tmp_t;
		terminals.insert(tmp_t);
	}

	Graph graph(n+1);

	for (int i = 1; i <= m; i++) {
		int v,w;
		std::cin >> v >> w;
		graph[v].push_back(w);
		graph[w].push_back(v);
	}

	int opt;
	std::cin >> opt;

	auto p = reduceNeighborhoods(mp(graph,opt),terminals);
	graph = p.first;
	opt = p.second;

	for (int t : terminals) for (int t1 : terminals)
		if (t1 != t)
			for (int v : graph[t]) for (int w : graph[t1])
				if (v == w)
					std::cout << "Wspólny somsiad.\n";
	int n_size = 0;
	for (int t : terminals)
		n_size += graph[t].size();

	if (2*opt < n_size)
		std::cout << "OPT nie bangla: " << opt << " vs " << n_size << "\n";

	if (terminals.size() == 3)
		std::cout << (opt*n_size*(n_size-1)*(n_size-2))/6 << "\n";
	else
		std::cout << (opt*n_size*(n_size-1)*(n_size-2)*(n_size-3))/24 << "\n";

	return 0;

}
