#include <iostream>

#include "basic.h"
#include "graph.h"
#include "mwc.h"

std::pair<Graph,int> reduceNeighborhoods(std::pair<Graph,int> input,VertexSet const& terminals) {
	auto [graph,opt] = input;
	
	for (int t : terminals) for (int t1 : terminals)
		if (t1 != t)
			for (int v : graph[t]) for (int w : graph[t1])
				if (v == w)
					return reduceNeighborhoods(mp(removeVertex(move(graph),v),opt-1),terminals);

	return input;
}

int main() {
	int n,m,t;
	std::cin >> n >> m >> t;

	VertexSet terminals;

	for (int i = 1; i <= t; i++) {
		int tmp_t;
		std::cin >> tmp_t;
		terminals.insert(tmp_t);
	}

	Graph graph(n+1);

	for (int i = 1; i <= m; i++) {
		int v,w;
		std::cin >> v >> w;
		graph[v].push_back(w);
		graph[w].push_back(v);
	}

	int opt;
	std::cin >> opt;

	auto p = reduceNeighborhoods(mp(graph,opt),terminals);
	graph = p.first;
	opt = p.second;

	auto [kernel,del_vcs] = toMWCForm(MWC::getKernel(toCanForm(mp(graph,terminals)),opt));
  int m2 = 0;
	for (int v = 0; v < (int)kernel.size(); v++)
		for (int w : kernel[v])
			if (v < w)
        m2++;

	std::cout << n << " " << m2 << " " << t << std::endl;
	for (int terminal : terminals)
		std::cout << terminal << " ";
	std::cout << std::endl;

	for (int v = 0; v < (int)kernel.size(); v++)
		for (int w : kernel[v])
			if (v < w)
				std::cout << v << " " << w << std::endl;

	std::cerr << del_vcs << std::endl;
}

