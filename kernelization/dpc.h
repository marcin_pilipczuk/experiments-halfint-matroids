#include <vector>
#include <algorithm>

#include "graph.h"

namespace DPC {
	struct Instance {
		Graph digraph;
		int s;
		std::vector<std::pair<int,int>> pairs;
		int k;
	};

	std::pair<Instance,VetexSet> getKernel(Instance I);
};

