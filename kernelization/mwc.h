#include <vector>
#include <set>
#include <algorithm>

#include "graph.h"

#ifndef MWC_H

#define MWC_H

namespace MWC {
	std::pair<Graph,int> getKernel(std::pair<Graph,VertexSet>&&,int);
	bool isSolValid(Graph const&,VertexSet const&,VertexSet const&);
}

std::pair<Graph,VertexSet> toCanForm(std::pair<Graph,VertexSet>&&);
//std::pair<Graph,VertexSet> toMWCForm(std::pair<Graph,VertexSet>&&);

std::pair<Graph,int> toMWCForm(std::pair<Graph,int>&&);

#endif // MWC_H

