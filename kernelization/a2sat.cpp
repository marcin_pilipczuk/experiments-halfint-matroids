#include <iostream>
#include <cassert>
#include <stack>
#include <vector>
#include <queue>

#include "a2sat.h"

#define asVertex(a) (((a)<0) ? (n+(a)) : (a))
#define MAX_N 200001

using namespace std;

//
// Conversion to graph
//

Graph A2SAT::toGraph() {
	Graph result(n);
	for (auto clause : clauses) {
		result[asVertex(-clause.first)].push_back(asVertex(clause.second));
		result[asVertex(-clause.second)].push_back(asVertex(clause.first));
	}
	return result;
}

//
// Approx solution
//

int A2SAT::getBadVar() {
	auto [component,graph] = getSSS(this->toGraph());

	for (int x = 1; x <= n; x++)
		if (component[asVertex(x)] == component[asVertex(-x)])
			return x;

	return -1;
}

void A2SAT::removeVar(int x) {
	vector<Clause> res_clauses;
	for (auto clause : clauses)
		if (clause.first != x and clause.first != -x and clause.second != x and clause.second != -x)
			res_clauses.push_back(clause);
	this->clauses = res_clauses;
}	

pair<VertexSet,A2SAT> A2SAT::getApproxSolution() {
	VertexSet result;
	A2SAT instance_copy = (*this);

	int x = instance_copy.getBadVar();
	while (x != -1) {
		result.insert(x);
		instance_copy.removeVar(x);
	}

	return mp(result,instance_copy);
}

//
// Make zero positive
//

void A2SAT::negateVariableSet(VertexSet vars) {
	for (auto & clause : clauses) {
		if (vars.count(clause.first) > 0 or vars.count(-(clause.first)) > 0)
			clause.first = -clause.first;
		if (vars.count(clause.second) > 0 or vars.count(-(clause.second)) > 0)
			clause.second = -clause.second;
	}
}

//
// DANGEROUS - olny solvable instances
//

vector<bool> A2SAT::get2SATSolution() {
	auto [scc_comp,scc_graph] = getSSS(toGraph());
	vector<int> opp_comp(scc_graph.size(),-1);

	for (int x = 1; x <= n; x++) {
		assert(opp_comp[scc_comp[asVertex(x)] != scc_comp[asVertex(-x)]]);
		opp_comp[scc_comp[asVertex(x)]] = scc_comp[asVertex(-x)];
		opp_comp[scc_comp[asVertex(-x)]] = scc_comp[asVertex(x)];
	}

	vector<int> degree(scc_graph.size(),0);
	for (int v = 1; v < scc_graph.size(); v++)
		for (int w : scc_graph[v])
			degree[w]++;

	queue<int> Q;
	for (int v = 1; v < scc_graph.size(); v++)
		if (degree[v] == 0)
			Q.push(v);

	vector<bool> comp_val(scc_graph.size(),-1);
	while (not Q.empty()) {
		int cur_comp = Q.front(); Q.pop();
		if (comp_val[cur_comp] != -1) {
			comp_val[cur_comp] = 0;
			comp_val[opp_comp[cur_comp]] = 1;

			for (int comp : scc_graph[cur_comp]) {
				degree[comp]--;
				if (degree[comp] == 0)
					Q.push(comp);
			}
		}
	}

	vector<bool> result(n+1,false);
	for (int v = 1; v <= n; v++)
		if (comp_val[scc_comp[asVertex(v)]] == 1)
			result[v] = true;
	return result;
}

//
// Instance to zero positive one -- WARNINNG -- only solvable
//

A2SAT A2SAT::getZeroPositive(A2SAT & solvable_subinstance) {
	A2SAT result = *this;

	auto sol_2sat = solvable_subinstance.get2SATSolution();

	VertexSet positive_vars;
	for (int x = 1; x <= n; x)
		if (sol_2sat[x])
			positive_vars.insert(x);

	result.negateVariableSet(positive_vars);

	return result;
}

//
// To DPC -- WARNING -- zero positive instances only
//

DPC::Instance A2SAT::toDPC(VertexSet const & some_solution) {
	DPC::Instance result;

	result.digraph = Graph(1 + n + some_solution.size() + 1);
	result.s = ((int)result.digraph.size())-1;
	
	// map vars
	vector<pair<int,int>> var_map;
	int last_num = 0;

	for (int v : some_solution)
		var_map[v] = mp(++last_num,++last_num);
	for (int v = 1; v <= n; v++)
		var_map[v] = mp(++last_num,-1);

	assert(last_num == ((int)result.digraph.size())-2);
	
	// add source edges
	for (int v : some_solution) {
		result.digraph[result.s].push_back(var_map[v].first);
		result.digraph[result.s].push_back(var_map[v].second);
		result.pairs.push_back(var_map[v]);
	}

	// convert clauses
	for (auto clause : clauses) {
		int x = clause.first;
		int y = clause.second;
		int xy_case = some_solution.count(abs(x)) + some_solution.count(y);

		if (xy_case  == 0) {
			if (x*y < 0)
				result.digraph[var_map[min(x,y)].first].push_back(var_map[max(x,y)].first);
			else
				result.pairs.push_back(mp(var_map[abs(x)].first,var_map[abs(y)].first));
		} else if (xy_case == 1) {
			if (x < 0 and y > 0)
				result.digraph[var_map[-x].second].push_back(var_map[y].first);
			else if (x > 0 and y < 0)
				result.pairs.push_back(mp(var_map[x].first,var_map[-y].first));
			else if (x > 0 and y > 0)
				result.digraph[var_map[x].first].push_back(var_map[y].first);
			else
				result.pairs.push_back(mp(var_map[-x].second,var_map[-y].first));
		} else {
			result.pairs.push_back(mp(
				(x > 0 ? var_map[x].first : var_map[-x].second),
				(y > 0 ? var_map[y].first : var_map[-y].second)
			));
		}
	}

	return result;
}

A2SAT::A2SAT(DPC::Instance const& I) {
	assert(false);
}

//
// Kernel
//

pair<A2SAT,VertexSet> A2SAT::getKernel() {
	auto [ approx_sol,solvable_2sat_instance ] = getApproxSolution();
	A2SAT zp_instance = getZeroPositive(solvable_2sat_instance);
	auto kernel = DPC::getKernel(zp_instance.toDPC(approx_sol));
}

//
// Input/output
//

istream & operator>>(istream & stream, A2SAT & I) {
	int m;
	stream >> I.n >> m;

	A2SAT::Clause tmp_clause;
	for (int i = 1; i <= m; i++) {
		stream >> tmp_clause.first >> tmp_clause.second;
		I.clauses.push_back(tmp_clause);
	}

	return stream;
}

ostream & operator<<(ostream & stream, A2SAT const & I) {
	stream << I.n << " " << I.clauses.size() << endl;

	for (auto clause : I.clauses)
		stream << clause.first << " " << clause.second;

	return stream;
}

A2SAT::A2SAT(Graph const & graph) {
	vector<pair<int,int>> matching = maxMatching(graph);

	vector<pair<int,int>> clauses;

	VertexSet matched_verts, unmatched_verts;
	for (auto p : matching) {
		matched_verts.insert(p.first);
		matched_verts.insert(p.second);
	}
	for (int v = 1; v < graph.size(); v++)
		if (matched_verts.count(v) == 0)
			unmatched_verts.insert(v);

	int n = 0;
	vector<int> vtx_map(graph.size());
	for (auto p : matching) {
		n++;
		vtx_map[p.first] = n;
		vtx_map[p.second] = -n;
	}
	for (int v : unmatched_verts) {
		n++;
		vtx_map[v] = n;
		clauses.push_back(mp(-n,-n));
	}

	for (int v = 1; v < graph.size(); v++)
		for (int w : graph[v])
			if (v < w)
				clauses.push_back(mp(vtx_map[v],vtx_map[w]));
}

