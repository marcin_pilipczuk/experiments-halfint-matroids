
#include "cassert"

#include "inc_tuples.h"

using namespace std;

MyTuples::_iter::_iter(vector<int> t, int _k) :  k(_k),actual_tuple(t) {}

MyTuples::_iter MyTuples::_iter::operator++() {
	for (int i = (int)actual_tuple.size() - 1; i >= 1; i--)
		if (actual_tuple[i] < k) {
			actual_tuple[i]++;
			for (int j = i+1; j < (int)actual_tuple.size(); j++)
				actual_tuple[j] = actual_tuple[i];
			return *this;
		}

	actual_tuple[0]++;
	for (int i = 1; i < (int)actual_tuple.size(); i++)
		actual_tuple[i] = actual_tuple[0];

	return *this;
}

vector<int> MyTuples::_iter::operator*() {
	return actual_tuple;
}

MyTuples::_iter MyTuples::begin() {
	return _iter(vector<int>(n,0),k);
}

MyTuples::_iter MyTuples::end() {
	return _iter(vector<int>(n,k+1),k);
}

MyTuples::MyTuples(int _n, int _k) : n(_n), k(_k) {}

bool operator==(MyTuples::_iter const & A, MyTuples::_iter const & B) {
	return A.actual_tuple == B.actual_tuple;
}

bool operator!=(MyTuples::_iter const & A, MyTuples::_iter const & B) {
	return A.actual_tuple != B.actual_tuple;
}

