
#ifndef MY_TUPLES_H
#define MY_TUPLES_H

#include <vector>

class MyTuples {
	int n,k;

public:
	class _iter {
		int k;
		std::vector<int> actual_tuple;
	public:
		friend bool operator==(_iter const&,_iter const&);
		friend bool operator!=(_iter const&,_iter const&);
		_iter(std::vector<int> t,int k);
		_iter operator++();
		std::vector<int> operator*();
	};

	_iter begin();
	_iter end();

	MyTuples(int,int);
};

#endif // MY_TUPLES_H
