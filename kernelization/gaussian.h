#include <vector>

#include "field.h"

#ifndef GAUSSIAN_H

#define GAUSSIAN_H


class IndependentSet {
private:
	std::vector<std::vector<Field::Element>> matrix;
	std::vector<int> colsMap;

	void swapCols(int A, int B);
	Field::Element getVal(int,int);
	void setVal(int,int,Field::Element);
	int findNonZeroInRow(int x, int y);

public:
	bool addVector(std::vector<Field::Element> row);
};


std::pair<Matrix,std::vector<int>> reduceMatrix(Matrix && rows);
std::vector<int> getSignificantRows(Matrix && family_rows);
int getFirstZeroRow(Matrix && family_rows,int);

#endif // GAUSSIAN_H

