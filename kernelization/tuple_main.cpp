
#include <iostream>

#include "inc_tuples.h"

using namespace std;

int main() {
	for (auto t : MyTuples(3,3)) {
		for (int v : t)
			cout << v << " ";
		cout << endl;
	}
}
