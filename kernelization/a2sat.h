#include <vector>
#include <set>
#include <algorithm>
#include <iostream>

#include "graph.h"
#include "dpc.h"

class A2SAT {
	friend std::istream & operator>>(std::istream &, A2SAT &);
	friend std::ostream & operator<<(std::ostream &, A2SAT const&);

private:
	typedef std::pair<int,int> Clause;

	int n;
	std::vector<Clause> clauses;

	Graph toGraph();

	int getBadVar();
	void removeVar(int x);
	std::pair<VertexSet,A2SAT> A2SAT::getApproxSolution();

	DPC::Instance toDPC(VertexSet const &);
	A2SAT(DPC::Instance const&);
	A2SAT(Graph const &);

	void negateVariableSet(VertexSet vars);
	A2SAT getZeroPositive(A2SAT &);
	std::vector<bool> A2SAT::get2SATSolution();
	void makeZeroPositive(VertexSet const &);
	
	VertexSet compress(VertexSet const & approx_sol);


public:
	std::pair<A2SAT,VertexSet> getKernel();
};

std::istream & operator>>(std::istream &, A2SAT &);
std::ostream & operator<<(std::ostream &, A2SAT &);


