#include <sstream>
#include <queue>
#include <functional>
#include <iostream>

#include "graph.h"
#include "matroid.h"
#include "mwc.h"
#include "gammoid.h"
#include "cut_covering.h"
#include "gaussian.h"
#include "inc_tuples.h"
#include "min_vertex_cut.h"

//#define DEBUG

using namespace std;

//
// Remove empty vertices
//

pair<Graph,vector<int>> removeEmptyVertices(Graph && graph) {
	vector<int> vmap(graph.size(),-1);
	int next_free = 1;

	for (int v = 0; v < (int)graph.size(); v++)
		if ((int)graph[v].size() > 0)
			vmap[v] = next_free++;

	Graph result{vector<int>{}};
	for (int v = 0; v < (int)graph.size(); v++)
		if ((int)graph[v].size() > 0)
			result.push_back(mapf(graph[v],function<int(int)>([=](int x){return vmap[x];})));

	return mp(result,vmap);
}

//
// Add sinks
//

pair<Graph,vector<int>> addSinks(Graph my_graph, VertexSet sinks) {
	vector<int> sink_copy(my_graph.size(),0);
	for (int v : sinks) {
		sink_copy[v] = my_graph.size();
		my_graph.push_back(vector<int>());
	}

	for (int v = 1; v < ((int)sink_copy.size()); v++) {
		int k = my_graph[v].size();
		for (int i = 0; i < k; i++)
			if (sink_copy[my_graph[v][i]] != 0)
				my_graph[v].push_back(sink_copy[my_graph[v][i]]);
	}

	return mp(my_graph,sink_copy);
}

//
// Preprocessing routine
//

vector<Field::Element> getRepVec(vector<Field::Element> uni_vec, vector<Field::Element> gammoid_vec, int n) {
	vector<Field::Element> result;
	for (auto e : uni_vec)
		for (auto t : MyTuples(n,((int)gammoid_vec.size())-1)) {
			for (int i = 0; i < n; i++)
				e *= gammoid_vec[t[i]];
			result.push_back(e);
		}

	return result;
}

pair<Graph,int> MWC::getKernel(pair<Graph,VertexSet> && input, int approx_sol_size) {
	Graph graph = input.first;
	VertexSet terminals = input.second;

	// get terminal neighborhood

	VertexSet N_terminals;
	for (auto terminal : terminals)
		for (int v : graph[terminal])
			N_terminals.insert(v);

	// create uniform matroid
	
	auto uni_rep = getUniformMatroidRep((int)graph.size()*2+1,approx_sol_size);

	// get undeletable set
	
	int counter = 0;

	while (true) {
		// create gammoids

		vector<int> V_casual;
		for (int v = 0; v < (int)graph.size(); v++)
			if (terminals.count(v) == 0 and N_terminals.count(v) == 0 and graph[v].size() > 0l)
				V_casual.push_back(v);
		
		vector<int> vert_map;
		for (int v : N_terminals)
			vert_map.push_back(v);
		for (int v : V_casual)
			vert_map.push_back(v);
		
		auto [ G_with_sources, sources ] = addSourcesBefore(graph,N_terminals);
		auto [ G_prim, sinks ] = addSinks(G_with_sources,set<int>(vert_map.begin(),vert_map.end()));
		auto gammoid_rep = getGammoidRep(G_prim,sources);

		// create rep vectors

		bool success = false;
		IndependentSet rep_vecs;
		for (int i = 0; i < (int)vert_map.size(); i++) {
			int v = vert_map[i];
			bool indep = rep_vecs.addVector(getRepVec(uni_rep[v-1],gammoid_rep[sinks[v]-1],terminals.size()));
			if (not indep and i >= (int)N_terminals.size()) {
				graph = bypassVertex(move(graph),vert_map[i]);
        std::cerr << "Bypassing vertex " << vert_map[i] << std::endl;
				success = true;
				break;
			}
		}

		if (not success)
			break;
		else
			counter++;
	}

	return mp(graph,counter);
//	auto [res_graph,res_map] = removeEmptyVertices(move(graph));
//	VertexSet res_terminals;
//	for (int t : terminals)
//		res_terminals.insert(res_map[t]);
//	return mp(res_graph,res_terminals);
}

// Instance forms

pair<Graph,VertexSet> toCanForm(pair<Graph,VertexSet> && instance) {
	auto graph = instance.first;
	auto terminals = instance.second;

	for (int v = 0; v < (int)graph.size(); v++)
		for (int & w : graph[v])
			w++;
	Graph new_graph{std::vector<int>{}};
	new_graph.insert(new_graph.end(),graph.begin(),graph.end());

	VertexSet new_terminals;
	for (int v : terminals)
		new_terminals.insert(v+1);
	return mp(new_graph,new_terminals);
}
//
//pair<Graph,VertexSet> toMWCForm(pair<Graph,VertexSet> && instance) {
//	auto graph = instance.first;
//	auto terminals = instance.second;
//
//	for (int v = 1; v < (int)graph.size(); v++)
//		for (int & w : graph[v])
//			w--;
//	Graph new_graph;
//	new_graph.insert(new_graph.end(),next(graph.begin()),graph.end());
//
//	VertexSet new_terminals;
//	for (int v : terminals)
//		new_terminals.insert(v-1);
//
//	return mp(new_graph,new_terminals);
//}

pair<Graph,int> toMWCForm(pair<Graph,int> && instance) {
	auto graph = instance.first;

	for (int v = 1; v < (int)graph.size(); v++)
		for (int & w : graph[v])
			w--;
	Graph new_graph;
	new_graph.insert(new_graph.end(),next(graph.begin()),graph.end());

	return mp(new_graph,instance.second);
}

