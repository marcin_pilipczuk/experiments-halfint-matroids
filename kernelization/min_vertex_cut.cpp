#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <list>
#include <set>
#include <stack>
#include <unordered_map>

#include "basic.h"

using namespace std;

//
// Relabelling heuristic
//

void heura(vector<unordered_map<int,unsigned long long>> const & Network, vector<int> & h, stack<int> & FQ, int n, int s, int t) {
	queue<int> HQ;

	for (int i = 1; i <= n; i++)
		if (i != s && i != t)
			h[i] = INF;

	HQ.push(t);

	while (!HQ.empty()) {
		int current = HQ.front(); HQ.pop();
		FQ.push(current);
		for (auto p : Network[current])
			if (Network.at(p.first).at(current) != 0L and h[p.first] == INF && p.first != s) {
				h[p.first] = h[current]+1;
				HQ.push(p.first);
			}
	}
}

//
// Pushing vertex
//

long long singlePush(vector<unordered_map<int,unsigned long long>> & Network, vector<unsigned long long> & charge, int s, int t) {
	long long amount = min(charge[s],Network[s][t]);

	charge[t] += amount;
	charge[s] -= amount;
	Network[s][t] -= amount;
	Network[t][s] += amount;

	return amount;
}

long long push(vector<unordered_map<int,unsigned long long>> & Network, vector<int> & h, vector<unsigned long long> & charge, int v) {
	long long res = 0L;
	for (auto p : Network[v]) {
		if (Network[v][p.first] and h[v] == h[p.first]+1)
			res += singlePush(Network,charge,v,p.first);
		if (!charge[v])
			return res;
	}
	return res;
}

//
// finding reachable vertices in network
//

void _reachableInNetwork(vector<unordered_map<int,unsigned long long>> Network, int v, vector<bool> & visited) {
	if (visited[v])
		return;
	visited[v] = true;

	for (auto p : Network[v])
		if (p.second != 0L)
			_reachableInNetwork(Network,p.first,visited);
}

vector<bool> reachableInNetwork(vector<unordered_map<int,unsigned long long>> Network, int s) {
	vector<bool> result((int)Network.size(),false);
	_reachableInNetwork(Network,s,result);
	return result;
}

//
// Compute min cut
//

pair<long long,vector<pair<int,int>>> computeMaxFlow(vector<unordered_map<int,unsigned long long>> Network, int s, int t) {
	int n = (int)Network.size()-1;

	vector<int> h(n+1,0);
	h[s] = INF;

	vector<unsigned long long> charge(n+1,0L);

	stack<int> FQ;
	for(auto p : Network[s]) {
		charge[p.first] = p.second;
		FQ.push(p.first);
	}

	int addition = -1;

	while (addition) {
		heura(Network,h,FQ,n,s,t);

		addition = 0;
		while (!FQ.empty()) {
			int v = FQ.top(); FQ.pop();
			addition += push(Network,h,charge,v);
		}
	}

	auto reachable = reachableInNetwork(Network,s);
	vector<pair<int,int>> result;
	for (int v = 1; v < (int)Network.size(); v++)
		for (auto p : Network[v])
			if (reachable[v] and reachable[p.first])
				result.push_back(mp(v,p.first));

	return mp(charge[t],result);
}

//
// Prepare network
//

//
// in_v : 2*v-1
// out_v : 2*v
// original : (v+1)/2
//

VertexSet minVertexCut(Graph const& graph, VertexSet const& S, VertexSet const& T) {
	vector<unordered_map<int,unsigned long long>> Network(2*graph.size());

	for (int v = 1; v < ((int)graph.size()); v++) {
		Network[2*v-1][2*v] = 1L;

		for (int w : graph[v]) {
			Network[2*v][2*w-1] = 1L;
		}
	}

	int s = (int)Network.size()-1;
	for (int v : S)
		Network[s][2*v-1] = INF;

	int t = (int)Network.size()-1;
	for (int v : T)
		Network[2*v][t] = INF;

	auto [flow,edges] = computeMaxFlow(Network,s,t);

	VertexSet result;

	for (auto e : edges)
		result.insert((e.second+1)/2);

	return result;
}

