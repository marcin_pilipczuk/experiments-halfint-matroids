#include "matroid.h"
#include "gammoid.h"
#include "graph.h"
#include "dpc.h"

using namespace std;

DPC::Instance reducePairs(DPC::Instance I) {
	auto [_G,_S] = copySourceKTimes(I.digraph,I.s,I.k);
	auto [G,copy_map] = duplicateGraph(_G);

	VertexSet S;
	for (int s : _S){
		S.insert(s);
		S.insert(copy_map[s]);
	}

	vector<vector<int>> P;
	for (auto p : I.pairs)
		P.push_back(vector<int>{p.first-1,p.second+n-1});

	auto P_res = getMatroidRepSet(getGammoidRep(G,S),P);

	I.pairs = vector<pair<int,int>>();
	for (auto p : P_res)
		I.pairs.push_back(mp(p[0]+1,[p[1]-n+1]));

	return I;
}

VertexSet getIrrelevantVertices(DPC::Instance const & I) {
	
}

DPC::Instance reduceIrrelevant(DPC::Instance && I, VertexSet const & irr_verts) {
	
}

pair<DPC::Instance,VetexSet> getKernel(DPC::Instance I) {
	I = reducePairs(move(I));
	auto irr_verts = getIrrelevantVertices(I);
	return mp(reduceIrrelevant(move(I),irr_verts),irr_verts);
}
