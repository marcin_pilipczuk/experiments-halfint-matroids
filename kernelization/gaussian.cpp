#include <vector>
#include <cstdio>
#include <iostream>
#include <cassert>

#include "basic.h"
#include "gaussian.h"

using namespace std;

#define max(a,b) (a>b?a:b)

struct GaussianEliminator {
	vector<vector<Field::Element>> & matrix;
	int rows, cols;

	vector<int> rowsMap;
	vector<int> colsMap;

	GaussianEliminator(vector<vector<Field::Element>> & _matrix) :
		matrix(_matrix),
		rows(_matrix.size()),
		cols(_matrix[0].size())
	{
		rowsMap = vector<int>(rows);
		colsMap = vector<int>(cols);

		for(int i = 0; i < rows; i++)
			rowsMap[i] = i;
		for(int i = 0; i < cols; i++)
			colsMap[i] = i;
	}

	int findNonZeroInRow(int row, int begin) {
		for (int i = begin; i < cols; i++)
			if (getVal(row,i) != 0)
				return i;
		return -1;
	}

	vector<int> getColsMap() {
		return colsMap;
	}

	void swapCols(int A, int B) {
		if (A == B)
			return;

		colsMap[A] ^= colsMap[B];
		colsMap[B] ^= colsMap[A];
		colsMap[A] ^= colsMap[B];
	}

	void swapRows(int A, int B) {
		if (A == B)
			return;

		rowsMap[A] ^= rowsMap[B];
		rowsMap[B] ^= rowsMap[A];
		rowsMap[A] ^= rowsMap[B];
	}

	void normalize_row(int row, int col) {
		auto inv_val = getVal(row,col).inv();
		for (int i = col; i < cols; i++)
			setVal(row,i,getVal(row,i)*inv_val);
	}

	void subRowFromAllBelow(int row, int begin) {
		for (int i = row+1; i < rows; i++)
			if (not getVal(i,begin) == 0) {
				auto mult = getVal(i,begin);
				for(int j = begin; j < cols; j++)
					setVal(i,j,getVal(i,j)-(getVal(row,j)*mult));
			}
	}

	inline Field::Element getVal(int r, int c) {
		return matrix[rowsMap[r]][colsMap[c]];
	}

	inline void setVal(int r, int c, Field::Element val) {
		matrix[rowsMap[r]][colsMap[c]] = val;
	}

	void standardElimination() {
		int currentRow = 0, currentCol = 0;
		while (currentRow < rows && currentCol < cols) {
			if (getVal(currentRow,currentCol) != 0) {
				if (getVal(currentRow,currentCol) != 1)
					normalize_row(currentRow,currentCol);

				subRowFromAllBelow(currentRow,currentCol);

				currentRow++;
				currentCol++;
			} else {
				int zero = findNonZeroInRow(currentRow,currentCol);
				if(zero != -1) {
					swapCols(currentCol, zero);
				} else {
					swapRows(currentRow, rows-1);
					rows--;
				}
			}
		}
	}

	void fullReduction() {
		standardElimination();

		for (int row = rows-1; row >= 1; row--)
			for (int cur_row = row-1; cur_row >= 0; cur_row--) {
				auto val = getVal(cur_row,row);
				setVal(cur_row,row,0);
				for (int i = rows; i < cols; i++)
					setVal(cur_row,i,getVal(cur_row,i)-val*getVal(row,i));
			}
	}

	vector<int> getSignificantRows() {
		standardElimination();

		vector<int> result;
		for (int i = 0; i < min(cols,rows); i++)
			result.push_back(rowsMap[i]);

		return result;
	}

	int getFirstZeroRow(int forbidden) {
		int currentRow = 0, currentCol = 0;
		while (currentRow < rows and currentCol < cols) {
			if (getVal(currentRow,currentCol) != 0) {
				if (getVal(currentRow,currentCol) != 1)
					normalize_row(currentRow,currentCol);

				subRowFromAllBelow(currentRow,currentCol);

				currentRow++;
				currentCol++;
			} else {
				int zero = findNonZeroInRow(currentRow,currentCol);
				if(zero != -1)
					swapCols(currentCol, zero);
				else if (currentRow < forbidden)
					currentRow++;
				else
					return currentRow;
			}
		}

		if (currentRow < rows)
			return currentRow;
		else
			return -1;
	}

	Matrix getMatrix() {
		Matrix result(rows,vector<Field::Element>(cols,Field::Element(0)));

		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				result[i][j] = getVal(i,j);

		return result;
	}

	bool addRow(vector<Field::Element> new_row) {
		matrix.push_back(new_row);
		int new_idx = (int)matrix.size() - 1;

		for (int i = 0; i < new_idx; i++) {
			auto fst = getVal(new_idx,i);
			for (int j = i; j < (int)matrix[0].size(); j++)
				setVal(new_idx,j,getVal(new_idx,j)-getVal(i,j)*fst);
		}

		auto new_col = findNonZeroInRow(new_idx,new_idx);
		if (new_col == -1) {
			matrix.pop_back();
			return false;
		}

		if (new_col != new_idx)
			swapCols(new_col,new_idx);

		//normalizuj !!

		return true;
	}
};

pair<Matrix,vector<int>> reduceMatrix(Matrix && rows) {
	GaussianEliminator eliminator(rows);
	eliminator.fullReduction();
	return mp(eliminator.getMatrix(),invPerm(eliminator.getColsMap()));
}

vector<int> getSignificantRows(Matrix && family_rows) {
	GaussianEliminator eliminator(family_rows);
	return eliminator.getSignificantRows();
}

int getFirstZeroRow(Matrix && family_rows, int forbidden) {
	GaussianEliminator eliminator(family_rows);
	return eliminator.getFirstZeroRow(forbidden);
}

//
// Getting vector of determinants
//

void IndependentSet::swapCols(int A, int B) {
	if (A == B)
		return;

	colsMap[A] ^= colsMap[B];
	colsMap[B] ^= colsMap[A];
	colsMap[A] ^= colsMap[B];
}

Field::Element IndependentSet::getVal(int x, int y) {
	return matrix[x][colsMap[y]];
}

void IndependentSet::setVal(int x, int y, Field::Element val) {
	matrix[x][colsMap[y]] = val;
}

int IndependentSet::findNonZeroInRow(int x, int y) {
	for (int i = y; i < (int)colsMap.size(); i++)
		if (getVal(x,i) != 0)
			return i;
	return -1;
}

bool IndependentSet::addVector(vector<Field::Element> row) { // only reduced matrix
	if ((int)matrix.size() == 0) {
		colsMap = vector<int>(row.size());
		for (int i = 0; i < (int)colsMap.size(); i++)
			colsMap[i] = i;
	}

	matrix.push_back(row);
	int new_idx = (int)matrix.size() - 1;

	for (int i = 0; i < new_idx; i++) {
		auto fst = getVal(new_idx,i);
		for (int j = i; j < (int)matrix[0].size(); j++)
			setVal(new_idx,j,getVal(new_idx,j)-getVal(i,j)*fst);
	}

	auto new_col = findNonZeroInRow(new_idx,new_idx);
	if (new_col == -1) {
		matrix.pop_back();
		return false;
	}

	if (new_col != new_idx)
		swapCols(new_col,new_idx);

	Field::Element inv_fst = getVal(new_idx,new_idx).inv();
	setVal(new_idx,new_idx,1L);
	for (int i = new_idx; i < (int)row.size(); i++)
		setVal(new_idx,i,getVal(new_idx,i)*inv_fst);
	return true;
}

Matrix transpose(Matrix const & matrix) {
	Matrix result(matrix[0].size(),vector<Field::Element>(matrix.size(),Field::Element(0)));

	for (int i = 0; i < (int)matrix.size(); i++)
		for (int j = 0; j < (int)matrix[0].size(); j++)
			result[j][i] = matrix[i][j];

	return result;
}

