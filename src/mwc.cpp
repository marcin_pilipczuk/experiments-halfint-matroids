//
// Created by malcin on 20.07.18.
//

#include <cstdio>
#include <cstring>

#include "mwc.h"
#include "mwc_tools.h"
#include "mwc_impseps.h"

int main(int argc, char *argv[]) {
    MWCSolver *solver;

    if (argc <= 1 || !strcmp(argv[1], "CSP")) {
        solver = new CSPMWCSolver();
    } else if (!strcmp(argv[1], "CSPLB1")) {
            solver = new CSPMWCSolverLB1();
    } else if (!strcmp(argv[1], "CSPLB2")) {
        solver = new CSPMWCSolverLB2();
    } else if (!strcmp(argv[1], "appx")) {
        solver = new MWCMinCutAppxSolver();
    } else if (!strcmp(argv[1], "check")) {
        solver = new MWCImpSepSolver();
    } else if (!strcmp(argv[1], "CSPpre")) {
        solver = new CSPMWCSolverWithPreprocessing();
    } else if (!strcmp(argv[1], "ImpSeps")) {
        solver = new MWCClassicSolver();
    } else {
        fprintf(stderr, "Wrong arguments\n");
        return -1;
    }

    // Works for CSP* and ImpSeps
    if (argc > 2 && !strcmp(argv[2], "CC")) {
        solver->use_split_cc = true;
    }

    solver->initialize();
    solver->solve();
    solver->print_solution();

    delete solver;
    return 0;
}

