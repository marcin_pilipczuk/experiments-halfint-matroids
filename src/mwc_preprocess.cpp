/*** Half-integral preprocessing for Multiway Cut
 * Input: takes a .mwc instance on STDIN
 * Output:
 *  - On STDOUT prints a reduced .mwc instance.
 *    The vertex set and terminal set are exactly the same, only set of vertices differ.
 *    Reduced vertices will be isolated in the output.
 *  - On STDERR summarizes the reduction in the following form. Each line is followed by a second line
 *    with the list of vertices in question. All listed vertices will be isolated in the output graph)
 *    DELETED: vertices assigned greedily to solution
 *    UNDELETABLE_VERTICES: vertices assigned to be not deleted (not in an optimum solution)
 *    RESOLVED_TERMINALS: terminals that become completely resolved (in their own connected component)
 */

#include "mwc.h"

int main(void) {
    CSPMWCSolver s;
    s.initialize();
    MWCPreprocessingOutput po = s.preprocess();

    po.output_graph();
    po.output_report();
    return 0;
}
