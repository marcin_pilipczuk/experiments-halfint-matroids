#include <cstdio>
#include <cstring>

#include "oct.h"

int main(int argc, char *argv[]) {
    OCTSolver *solver;

    if (argc <= 1 || !strcmp(argv[1], "CSP")) {
        solver = new CSPOCTSolver();
    } else if (!strcmp(argv[1], "CSPred")) {
        solver = new CSPOCTSolver(true);
    } else if (!strcmp(argv[1], "CSPLB1")) {
        solver = new CSPOCTSolverLB1();
    } else if (!strcmp(argv[1], "CSPLB2")) {
        solver = new CSPOCTSolverLB2();
    } else {
        fprintf(stderr, "Wrong arguments\n");
        return -1;
    }

    solver->initialize();
    solver->solve();
    solver->print_solution();

    delete solver;
    return 0;
}