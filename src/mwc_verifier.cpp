//
// Created by malcin on 19.07.18.
//

#include <cstdio>
#include <vector>

using std::vector;

vector<vector<int> > g;
vector<int> status;

int main(int argc, char *argv[]) {
    FILE *f;
    int opt = -1;

    if (argc > 3) {
        // OPT provided
        f = fopen(argv[3], "r");
        fscanf(f, "%d", &opt);
        fclose(f);
    }

    f = fopen(argv[1], "r");
    int n, m, t, a, b;
    fscanf(f, "%d%d%d", &n, &m, &t);
    g.resize(n);
    status.resize(n, 0);
    vector<int> q;
    for (int i = 1; i <= t; ++i){
        fscanf(f, "%d", &a);
        status[a] = i;
        q.push_back(a);
    }
    while(m--) {
        fscanf(f, "%d%d", &a, &b);
        g[a].push_back(b);
        g[b].push_back(a);
    }
    fclose(f);

    f = fopen(argv[2], "r");
    int sol_size;
    fscanf(f, "%d", &sol_size);
    for (int i = 0; i < sol_size; ++i) {
        fscanf(f, "%d", &a);
        if (a < 0 || a >= n) {
            printf("wrong vertex id: %d", a);
            return -1;
        } else if (status[a] < 0) {
            printf("vertex twice in solution: %d", a);
            return -1;
        } else if (status[a] > 0) {
            printf("terminal in solution: %d", a);
            return -1;
        }
        status[a] = -1;
    }
    fclose(f);
    while(!q.empty()) {
        int v = q.back();
        q.pop_back();
        for (auto w : g[v]) {
            if (status[w] == 0) {
                status[w] = status[v];
                q.push_back(w);
            } else if (status[w] > 0 && status[w] != status[v]) {
                printf("terminals #%d and #%d connected at vertices %d, %d", status[v], status[w], v, w);
                return -1;
            }
        }
    }
    printf("solution size=%4d", sol_size);
    if (opt >= 0) {
        printf(", opt=%4d", opt);
        if (opt > sol_size) {
            return 2;
        } else if (opt < sol_size) {
            return 1;
        } else {
            return 0;
        }
    } else {
        printf(", opt unknown");
        return 0;
    }
}
