//
// Created by malcin on 02.07.18.
//

#ifndef FVS_H
#define FVS_H

#include "csp_graph.h"
#include "solver.h"
#include "SolutionVariants.h"

// #define CHECK_REDUCTIONS 1

class CSPFVSSolution : public BranchingSolution {
public:
    CSPFVSSolution() : BranchingSolution() { root = -1; };
    CSPFVSSolution(int cap) : BranchingSolution(cap) { root = -1; };

    virtual Solution *clone(int cap) { return new CSPFVSSolution(cap); }

    virtual bool impossible(Graph &brG) {
        return  brG.degLowerBound() + (int)brG.getX().size() >= size_cap;
    }

    virtual vector<int> solve(Graph &g) {
        vector<int> apx = find_approx(g);
        if (!isSub || size_cap >= apx.size()) {
            size_cap = apx.size();
            sol = apx;
            solFound = true;
        }

        // May land here from recursion on connected components
        // Then we should look for undeletable guys
        set_undeletable_vertex(g);
        if (root >= 0)
            solve_pivot(g);
        else
            solve_no_pivot(g);

        return sol;
    }

    void solve_pivot(Graph &g) {
        assert(root >= 0);
        ReductionResult red = reduce(g);
        if (red == RED_NOSOLUTION) {
            return;
        }

        if (root < 0 || !g.degree(root)) {
            root = -1;
            solve_no_pivot(g);
            return;
        }

        assert(g.isInU(root));
        int pivot = choose_pivot_near_s(g, root);
        assert(pivot >= 0);

        int s = root;
        Graph g2(g);
        g.putInSolution(pivot);
        solve_pivot(g);

        root = s;
        g2.merge(root, pivot);
        solve_pivot(g2);
    }

    void solve_no_pivot(Graph &g) {
        ReductionResult r = reduce(g);
        if (r == RED_NOSOLUTION)
            return;

        pair<int, int> md = g.getMaxDegV();
        int s = md.first;

        if (s == -1)
            return;

        Graph g2(g);

        g.putInSolution(s);
        solve_no_pivot(g);

        g2.putInSafe(s);
        root = s;
        solve_pivot(g2);
    }

    virtual ReductionResult reductions(Graph &brG) {
        ReductionResult red = BranchingSolution::reductions(brG);
        if (red != RED_NOTHING) {
            return red;
        }
        set_undeletable_vertex(brG);
        if (root >= 0) {
#ifdef CHECK_REDUCTIONS
            Graph copy(brG);
#endif
            assert(brG.isInU(root));
            CSPGraph g(brG);
            Instance inst;
            g.make_fvs_instance(brG, root, inst);
            Solver solver(g, inst);
            CSPSolution sol = solver.solve();
            if ((int)brG.getX().size() + sol.lower_bound >= size_cap) {
                return RED_NOSOLUTION;
            }
            vector<int> safe = flood_safe(brG, g, sol);
            for (auto i : sol.ones) {
                if (brG.isNone(g.old_name(i))) {
                    red = RED_CHANGED;
                    brG.purePutInSolution(g.old_name(i));
                }
            }
            assert(brG.isInU(root));
            for (auto i : safe) {
                assert(brG.isNone(i) || i == root);
                if (brG.isNone(i)) {
                    red = RED_CHANGED;
                    brG.merge(root, i);
                }
            }
#ifdef CHECK_REDUCTIONS
            Sol8Full old_solver, new_solver;
            Graph copy2(copy), brG2(brG);
            vector<int> old_sol = old_solver.solve(copy2), new_sol = new_solver.solve(brG2);
            assert(old_sol.size() == new_sol.size());
#endif
        }
        return red;
    }

protected:

    void flood_safe_dfs(Graph &brG, vector<int> &res, vector<int> &vis, int x) {
        vis[x] = 1;
        res.push_back(x);
        int vis_seen = 0;
        for (int i = 0; i < brG.degree(x); ++i) {
            int y = brG.getNeighbours()[x][i];
            if (!vis[y] && (brG.isInU(y) || brG.isNone(y))) {
                flood_safe_dfs(brG, res, vis, y);
            }
            else if (vis[y] == 1) {
                vis_seen++;
            }
        }
        assert(vis_seen <= 1);
    }

    vector<int> flood_safe(Graph &brG, CSPGraph &g, CSPSolution &sol) {
        vector<int> res;
        vector<int> vis(brG.getN(), 0);
        for (auto a : sol.ones) {
            vis[g.old_name(a)] = 2;
        }
        for (auto a : sol.halves) {
            vis[g.old_name(a)] = 2;
        }
        flood_safe_dfs(brG, res, vis, root);
        return res;
    }

    long find_undeletable_vertex(Graph &brG) {
        int best = -1, best_deg = 0;
        for (int i = 0; i < brG.getN(); ++i) {
            if (brG.isInU(i) && brG.degree(i) > best_deg) {
                best = i;
                best_deg = brG.degree(i);
            }
        }
        return best;
    }

    void set_undeletable_vertex(Graph &brG) {
        if (root < 0 || !brG.isInU(root)) {
            root = find_undeletable_vertex(brG);
        }
    }

    int choose_pivot_near_s(Graph &brG, int s) {
        if (s < 0) {
            return -1;
        }
        int best = -1, best_deg = -1;
        for (int p = 0; p < brG.degree(s); ++p) {
            int x = brG.getNeighbours()[s][p];
            if (brG.isNone(x) && brG.degree(x) > best_deg) {
                best = x;
                best_deg = brG.degree(x);
            }
        }
        return best;
    }

    int root;
};

#endif //FVS_H
