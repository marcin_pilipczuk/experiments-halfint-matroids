//
// Created by malcin on 02.07.18.
//

#ifndef CSPSOLVER_VALUES_H
#define CSPSOLVER_VALUES_H

#include <cassert>
#include <vector>

using std::vector;
using std::pair;

typedef long value_t;

struct Instance {
    vector<pair<long, value_t> > init_vals;
};

class Constraint {
public:
    Constraint() {};
    ~Constraint() {};

    virtual bool isSatisfied(value_t a, value_t b) = 0;
    virtual bool isForcedLeft(value_t a) = 0;
    virtual bool isForcedRight(value_t b) = 0;
    virtual value_t forcedLeft(value_t a) = 0;
    virtual value_t forcedRight(value_t b) = 0;
};

class ORConstraint : public Constraint {
private:
    value_t ia, ib;
public:
    ORConstraint() {};
    ORConstraint(value_t _ia, value_t _ib) : ia(_ia), ib(_ib) {};

    virtual bool isSatisfied(value_t a, value_t b) {
        return a == ia || b == ib;
    }
    virtual bool isForcedLeft(value_t a) {
        return a != ia;
    }
    virtual bool isForcedRight(value_t b) {
        return b != ib;
    }
    virtual value_t forcedLeft(value_t a) {
        assert(isForcedLeft(a));
        return ib;
    }
    virtual value_t forcedRight(value_t b) {
        assert(isForcedRight(b));
        return ia;
    }
};

class PermutationConstraint : public Constraint {
protected:
    vector<value_t> perm_f, perm_r;
public:
    PermutationConstraint() {}
    PermutationConstraint(vector<value_t> &perm) {
        perm_f = perm;
        perm_r = vector<value_t>(perm.size());
        for (unsigned long i = 0; i < perm.size(); ++i) {
            perm_r[perm[i]] = i;
        }
    }
    virtual bool isForcedLeft(value_t a) {
        return true;
    }
    virtual bool isForcedRight(value_t b) {
        return true;
    }
    virtual value_t forcedLeft(value_t a) {
        assert(a >= 0 && a < perm_f.size());
        return perm_f[a];
    }
    virtual value_t forcedRight(value_t b) {
        assert(b >= 0 && b < perm_r.size());
        return perm_r[b];
    }

};

class EqualConstraint : public Constraint {
public:
    EqualConstraint() {};

    virtual bool isSatisfied(value_t a, value_t b) {
        return a == b;
    }
    virtual bool isForcedLeft(value_t a) {
        return true;
    }
    virtual bool isForcedRight(value_t a) {
        return true;
    }
    virtual value_t forcedLeft(value_t a) {
        return a;
    }
    virtual value_t forcedRight(value_t a) {
        return a;
    }
};

class BoolNeqConstraint : public Constraint {
public:
    BoolNeqConstraint() {}
    virtual bool isSatisfied(value_t a, value_t b) {
        assert(a == 0 || a == 1);
        assert(b == 0 || b == 1);
        return a != b;
    }
    virtual bool isForcedLeft(value_t a) {
        assert(a == 0 || a == 1);
        return true;
    }
    virtual bool isForcedRight(value_t a) {
        assert(a == 0 || a == 1);
        return true;
    }
    virtual value_t forcedLeft(value_t a) {
        assert(a == 0 || a == 1);
        return 1-a;
    }
    virtual value_t forcedRight(value_t a) {
        assert(a == 0 || a == 1);
        return 1-a;
    }

};

#endif //CSPSOLVER_VALUES_H
