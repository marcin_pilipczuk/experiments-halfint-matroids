#ifndef OCT_H
#define OCT_H

#include <cstdio>
#include <cstdlib>
#include "csp_graph.h"
#include "solver.h"

class OCTSolver {
protected:
    vector<long> best_solution, current_solution;
    bool solution_found;

public:

    OCTSolver() { solution_found = false; };
    virtual ~OCTSolver() {};

    vector<vector<long> > read_input() {
        int n, m, a, b;
        scanf("%d%d", &n, &m);
        vector<vector<long> > g(n);
        while(m--) {
            scanf("%d%d", &a, &b);
            g[a].push_back(b);
            g[b].push_back(a);
        }
        return g;
    }

    void print_solution(void) {
        printf("%lu\n", best_solution.size());
        for (auto a : best_solution) {
            printf("%ld ", a);
        }
        printf("\n");
    }

    virtual void initialize() = 0;
    virtual void solve() = 0;
};

class CSPOCTSolver : public OCTSolver {
protected:
    CSPGraph g;
    bool use_wernicke_reductions;
public:

    CSPOCTSolver() { solution_found = false; use_wernicke_reductions = false; };
    explicit CSPOCTSolver(bool _use_wernicke_reductions) : use_wernicke_reductions(_use_wernicke_reductions) {};
    virtual ~CSPOCTSolver() { }

    virtual void initialize() {
        vector<vector<long> > gi = read_input();
        g.make_oct_instance(gi);
    }

    virtual void solve() {
        branching(g);
    }

    void try_update_solution() {
        if (!solution_found || current_solution.size() < best_solution.size()) {
            best_solution = current_solution;
            solution_found = true;
        }
    }

    /* No advanced prunning here, only vertices that will be put into solution */
    virtual bool lower_bound_prunning(CSPSolution &s) {
        return solution_found && current_solution.size() + s.ones.size() >= best_solution.size();
    }

    void branching(CSPGraph &curr_g) {
        unsigned long sol_size = current_solution.size();
        if (use_wernicke_reductions) {
            curr_g.reduce_oct(current_solution);
        }
        if (solution_found && current_solution.size() >= best_solution.size()) {
            return;
        }
        Instance inst = curr_g.generate_instance();
        if (inst.init_vals.empty()) {
            long v = curr_g.get_one_none_vertex();
            if (v < 0) {
                try_update_solution();
                return;
            }
            inst.init_vals.push_back({v, 0});
            curr_g.set_value(v, 0);
        }
        Solver solver(curr_g, inst);
        CSPSolution s = solver.solve();

        if (lower_bound_prunning(s)) {
            return;
        }

        vector<long> forbidden(curr_g.get_n(), 0);
        for (auto a : s.halves) {
            forbidden[a] = 1;
        }
        for (auto a : s.ones) {
            forbidden[a] = 1;
            current_solution.push_back(a);
            curr_g.put_into_solution(a);
        }
        curr_g.flood_zeroes(forbidden);

        long pivot;
        if (s.halves.empty()) {
            pivot = curr_g.get_one_none_vertex();
            if (pivot < 0) {
                try_update_solution();
                return;
            }
            curr_g.set_value(pivot, 0); // There is no other valued vertex in the same connected component
        } else {
            pivot = s.halves.back();
        }

        assert(pivot >= 0 && pivot < curr_g.get_n());
        assert(curr_g.get_value(pivot) >= 0);

        CSPGraph copy(curr_g);

        curr_g.put_into_solution(pivot);
        current_solution.push_back(pivot);
        branching(curr_g);
        current_solution.pop_back();

        vector<long> conflicting = copy.put_into_undeletable(pivot, copy.get_value(pivot));
        for (auto a : conflicting) {
            current_solution.push_back(a);
            copy.put_into_solution(a);
        }
        branching(copy);

        while(current_solution.size() > sol_size) {
            current_solution.pop_back();
        }
    }
};

class CSPOCTSolverLB1 : public CSPOCTSolver {
public:
    CSPOCTSolverLB1() { solution_found = false; use_wernicke_reductions = false; }
    /* Standard half-integral lower bound */
    virtual bool lower_bound_prunning(CSPSolution &s) {
        return solution_found && current_solution.size() + s.ones.size() + (s.halves.size() + 1) / 2 >= best_solution.size();
    }
};

class CSPOCTSolverLB2 : public CSPOCTSolver {
public:
    CSPOCTSolverLB2() { solution_found = false; use_wernicke_reductions = false; }
    /* Iwata's improved half-integral lower bound */
    virtual bool lower_bound_prunning(CSPSolution &s) {
        return solution_found && current_solution.size() + s.lower_bound >= best_solution.size();
    }
};

#endif // OCT_H