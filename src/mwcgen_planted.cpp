// Test generator for Multiway cut with planted solution

#include<cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <random>
using std::vector;
using std::pair;
using std::swap;

int main(int argc, char *argv[]) {
    if (argc != 6) {
        printf("Usage: mwcgen_planted cutsize terms sidesize internal_edge_prob external_edge_prob\n");
        return -1;
    }

    int k = atoi(argv[1]), t = atoi(argv[2]), side = atoi(argv[3]);
    double iprob = atof(argv[4]), eprob = atof(argv[5]);

    std::random_device rand_dev;
    std::default_random_engine re(rand_dev());

    int n = k + side * t;
    vector<int> names(n);
    vector<pair<int, int> > edges;
    for (int i = 0; i < n; ++i) {
        names[i] = i;
        int j = std::uniform_int_distribution<int>(0, i)(re);
        swap(names[i], names[j]);
    }

    std::uniform_real_distribution<> rdist(0.0, 1.0);
    for (int a = 0; a < k; ++a) {
        for (int b = 0; b < a; ++b) {
            if (rdist(re) < iprob) {
                edges.push_back({t * side + a, t * side + b});
            }
        }
    }
    for (int i = 0; i < t; ++i) {
        for (int a = 0; a < side; ++a) {
            for (int b = 0; b < a; ++b) {
                if (rdist(re) < iprob) {
                    edges.push_back({i * side + a, i * side + b});
                }
            }
            for (int b = 0; b < k; ++b) {
                if (rdist(re) < eprob) {
                    edges.push_back({i * side + a, t * side + b});
                }
            }
        }
    }
    printf("%d %d %d\n", n, (int)edges.size(), t);
    for (int i = 0; i < t; ++i) {
        printf("%d ", names[i * side]);
    }
    printf("\n");
    for (auto e : edges) {
        printf("%d %d\n", names[e.first], names[e.second]);
    }
    return 0;
}