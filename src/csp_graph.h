//
// Created by malcin on 02.07.18.
//

#ifndef CSP_GRAPH_H
#define CSP_GRAPH_H

#include <vector>
#include <algorithm>
#include "values.h"
#include "Graph.h"

using std::vector;
using std::pair;

struct Edge {
    unsigned long id;
    long v;
    Constraint *cnst;

    Edge() {};
    Edge(unsigned long _id, long _v, Constraint *_cnst) : id(_id), v(_v), cnst(_cnst) {};
};

#define VAL_NONE -1
#define VAL_SOL -2

class CSPGraph {
private:
    vector<vector<Edge> > t;
    vector<value_t> assignment;

    ORConstraint *or_constraints;
    BoolNeqConstraint bn_constraint;
    EqualConstraint eq_constraint;
    unsigned long cnst_cnt;
    vector<long> old_names;
    vector<long> old2new;

    vector<long> marker;
    long marker_cnt;

    // For OCT reductions
    vector<long> vis, low;
    vector<pair<long, long> > edges_to_remove;
    vector<pair<long, long> > edge_stack;
    vector<value_t> tmp_assignment;
    vector<long> *stack;

    void remove_dart_by_index(long v, unsigned long p) {
        assert(v >= 0 && v <= t.size());
        assert(p >= 0 && p < t[v].size());
        unsigned long q = t[v].size();
        if (p < q - 1) {
            t[v][p] = t[v][q-1];
        }
        t[v].pop_back();
    }

    void remove_dart_by_eid(long v, unsigned long eid) {
        assert(v >= 0 && v <= t.size());
        for (unsigned long p = 0; p < t[v].size(); ++p) {
            if (t[v][p].id == eid) {
                remove_dart_by_index(v, p);
                return;
            }
        }
        assert(false);
    }

    void remove_vertex(long v) {
        assert(v >= 0 && v <= t.size());
        for (unsigned long p = 0; p < t[v].size(); ++p) {
            remove_dart_by_eid(t[v][p].v, t[v][p].id);
        }
        t[v].clear();
    }

    void dfs_connected_components(long v, long m, vector<long> &res) {
        res.push_back(v);
        marker[v] = m;
        for (auto e : t[v]) {
            if (marker[e.v] < m) {
                dfs_connected_components(e.v, m, res);
            }
        }
    }

    long oct_get_start(vector<long> &vs) {
        assert(!vs.empty());
        for (auto v : vs) {
            if (assignment[v] >= 0) {
                return v;
            }
        }
        return vs.back();
    }

    bool oct_dfs_bridges(long v, long &ind, long eid) {
        vis[v] = ind++;
        long min_hit = vis[v];
        bool res = (assignment[v] >= 0);
        for (unsigned long p = 0; p < t[v].size(); ++p) {
            Edge &e = t[v][p];
            if (vis[e.v] >= 0) {
                if (vis[e.v] < min_hit && eid != e.id) {
                    min_hit = vis[e.v];
                }
            } else {
                bool sub_res = oct_dfs_bridges(e.v, ind, e.id);
                if (low[e.v] < min_hit) {
                    min_hit = low[e.v];
                }
                if (low[e.v] > vis[v] && !sub_res) {
                    edges_to_remove.push_back({v, t[v][p].id});
                }
                res = (res || sub_res);
            }
        }
        low[v] = min_hit;
        return res;
    }

    bool oct_check_consistency(vector<long> &vs) {
        if (vs.empty()) {
            return true;
        }
        vector<long> valid(get_n(), 0);
        long s = oct_get_start(vs);
        for (auto v : vs) {
            tmp_assignment[v] = assignment[v];
            valid[v] = 1;
        }
        vector<long> q;
        if (tmp_assignment[s] < 0) {
            tmp_assignment[s] = 0;
        }
        q.push_back(s);
        while (!q.empty()) {
            long v = q.back();
            q.pop_back();
            for (auto e : t[v]) {
                if (valid[e.v]) {
                    if (tmp_assignment[e.v] == tmp_assignment[v]) {
                        return false;
                    }
                    if (valid[e.v] == 1) {
                        tmp_assignment[e.v]  = 1 - tmp_assignment[v];
                        valid[e.v] = 2;
                        q.push_back(e.v);
                    }
                }
            }
        }
        return true;
    }

    bool oct_dfs_leaf_2cc(long v, long &ind, long eid, vector<vector<long> > &leaf_cc) {
        vis[v] = ind++;
        long min_hit = vis[v];
        bool seen_cutvertex = false;
        for (unsigned long p = 0; p < t[v].size(); ++p) {
            Edge &e = t[v][p];
            if (vis[e.v] >= 0) {
                if (vis[e.v] < min_hit && eid != e.id) {
                    min_hit = vis[e.v];
                }
            } else {
                unsigned long pos = edge_stack.size();
                edge_stack.push_back({v, p});
                bool sub_res = oct_dfs_leaf_2cc(e.v, ind, e.id, leaf_cc);
                if (low[e.v] < min_hit) {
                    min_hit = low[e.v];
                }
                if (low[e.v] >= vis[v]) { // 2-connected component
                    if (!sub_res) {
                        leaf_cc.push_back(vector<long>());
                        for (unsigned long q = pos; q < edge_stack.size(); ++q) {
                            leaf_cc.back().push_back(neighbor(edge_stack[q].first, edge_stack[q].second));
                        }
                        leaf_cc.back().push_back(v);
                    }
                    while (edge_stack.size() > pos) {
                        edge_stack.pop_back();
                    }
                    seen_cutvertex = true;
                }
                seen_cutvertex = (seen_cutvertex || sub_res);
            }
        }
        low[v] = min_hit;
        return seen_cutvertex;
    }

public:

    CSPGraph() { or_constraints = NULL; marker_cnt = 0; };
    explicit CSPGraph(CSPGraph &g) { // Tailored for OCT and MwC
        t = g.t;
        assignment = g.assignment;
        bn_constraint = g.bn_constraint;
        or_constraints = NULL;
        if (!g.marker.empty()) {
            marker = vector<long>(get_n(), -1);
            marker_cnt = 0;
        }
    }

    explicit CSPGraph(Graph &g) { // Tailored for FVS
        int n = g.getN();
        old2new = vector<long>(n, -1);
        marker = vector<long>(n, -1);
        marker_cnt = 0;
        cnst_cnt = 0;
        for (int i = 0; i < n; ++i) {
            if (g.isNone(i)) {
                old2new[i] = old_names.size();
                old_names.push_back(i);
                cnst_cnt += g.degWithDouble(i);
            }
        }
        cnst_cnt /= 2;
        or_constraints = new ORConstraint[cnst_cnt];
        cnst_cnt = 0;
        t = vector<vector<Edge> >(old_names.size());
        for (long a = 0; a < old_names.size(); ++a) {
            int i = old_names[a];
            for (long p = 0; p < g.degree(i); ++p) {
                int j = g.getNeighbours()[i][p];
                long b = old2new[j];
                if (b >= 0 && i < j) {
                    for (int it = 0; it < (g.isDoubleEdge(i, p) ? 2 : 1); ++it) {
                        unsigned long eid = cnst_cnt++;
                        or_constraints[eid] = ORConstraint(eid, eid);
                        t[a].push_back(Edge(eid, b, &or_constraints[eid]));
                        t[b].push_back(Edge(eid, a, &or_constraints[eid]));
                    }
                }
            }
        }
    }

    // Induced subgraph for MWC
    CSPGraph(CSPGraph &g, vector<long> &cc) {
        unsigned long n = cc.size();
        assignment.resize(n);
        or_constraints = NULL;
        marker = vector<long>(n, -1);
        marker_cnt = 0;
        t.resize(n);
        old2new.resize(g.get_n(), -1);
        old_names = cc;
        for (long v = 0; v < (long)cc.size(); ++v) {
            old2new[cc[v]] = v;
            assignment[v] = g.assignment[cc[v]];
        }
        for (long v = 0; v < (long)cc.size(); ++v) {
            for (auto e : g.t[cc[v]]) {
                t[v].push_back(Edge(e.id, old2new[e.v], &eq_constraint));
            }
        }
    }

    ~CSPGraph() {
        delete[] or_constraints;
    }

    void make_fvs_instance(Graph &g, int s, Instance &inst) {
        assert(g.isInU(s));
        inst.init_vals.clear();
        unsigned long id = cnst_cnt + 1;
        for (long p = 0; p < g.degree(s); ++p) {
            int j = g.getNeighbours()[s][p];
            long b = old2new[j];
            if (b >= 0) {
                inst.init_vals.push_back({b, id++});
            }
        }
    }

    void make_oct_instance(vector<vector<long> > &g) {
        t = vector<vector<Edge > >(g.size());
        assignment = vector<value_t>(g.size(), VAL_NONE);
        marker = vector<long>(g.size(), -1);
        marker_cnt = 0;
        unsigned long eid = 0;
        for (long a = 0; a < g.size(); ++a) {
            for (long p = 0; p < g[a].size(); ++p) {
                if (g[a][p] > a) {
                    t[a].push_back(Edge(eid, g[a][p], &bn_constraint));
                    t[g[a][p]].push_back(Edge(eid, a, &bn_constraint));
                    eid++;
                }
            }
        }
    }

    vector<long> make_mwc_instance(long n, vector<pair<long, long> > &edges, vector<long> &terminals) {
        t = vector<vector<Edge> >(n);
        assignment = vector<value_t>(n, VAL_NONE);
        vector<long> status(n, -1), conflict;
        for (long p = 0; p < terminals.size(); ++p) {
            status[terminals[p]] = p;
        }
        unsigned long eid = 0;
        for (auto e : edges) {
            if (status[e.first] >= 0) {
                long x = e.first;
                e.first = e.second;
                e.second = x;
            }
            if (status[e.second] >= 0) {
                assert(status[e.first] == -1); // no edges connecting 2 terminals
                if (assignment[e.first] == VAL_NONE) {
                    assignment[e.first] = status[e.second];
                } else {
                    conflict.push_back(e.first);
                }
            } else {
                t[e.first].push_back(Edge(eid, e.second, &eq_constraint));
                t[e.second].push_back(Edge(eid, e.first, &eq_constraint));
                eid++;
            }
        }
        sort(conflict.begin(), conflict.end());
        conflict.erase(unique(conflict.begin(), conflict.end()), conflict.end());
        for (auto v : conflict) {
            put_into_solution(v);
        }
        return conflict;
    }

    void initialize_marker() {
        marker.resize(t.size(), -1);
        marker_cnt = 0;
    }

    inline value_t get_value(long v){
        assert(v >= 0 && v < t.size());
        return assignment[v];
    }

    inline void set_value(long v, value_t val) {
        assert(v >= 0 && v < t.size());
        assignment[v] = val;
    }

    inline void put_into_solution(long v) {
        assert(v >= 0 && v < t.size());
        assert(get_value(v) != VAL_SOL);
        set_value(v, VAL_SOL);
        remove_vertex(v);
    }

    inline vector<long> put_into_undeletable(long v, long val) {
        assert(v >= 0 && v < t.size());
        assert(get_value(v) != VAL_SOL);
        vector<long> conflicted;
        set_value(v, val);
        for (unsigned long p = 0; p < t[v].size(); ++p) {
            if (t[v][p].cnst->isForcedLeft(val)) {
                value_t b = t[v][p].cnst->forcedLeft(val);
                if (get_value(t[v][p].v) >= 0 && get_value(t[v][p].v) != b) {
                    conflicted.push_back(t[v][p].v);
                } else if (get_value(t[v][p].v) == VAL_NONE) {
                    set_value(t[v][p].v, b);
                }
            }
        }
        remove_vertex(v);
        return conflicted;
    }

    Instance generate_instance(void) {
        Instance inst;
        for (long v = 0; v < t.size(); ++v) {
            if (!t[v].empty() && get_value(v) >= 0) {
                inst.init_vals.push_back({v, get_value(v)});
            }
        }
        return inst;
    }

    long get_one_none_vertex(void) {
        for (long v = 0; v < t.size(); ++v) {
            if (!t[v].empty() && get_value(v) == VAL_NONE) {
                return v;
            }
        }
        return -1;
    }

    void flood_zeroes(vector<long> forbidden) {
        vector<long> q;
        for (long v = 0; v < t.size(); ++v) {
            if (!t[v].empty() && get_value(v) >= 0 && !forbidden[v]) {
                q.push_back(v);
            }
        }
        while (!q.empty()) {
            long v = q.back();
            assert(get_value(v) >= 0);
            q.pop_back();
            for (unsigned long p = 0; p < t[v].size(); ++p) {
                Edge &e = t[v][p];
                if (get_value(e.v) == VAL_NONE && e.cnst->isForcedLeft(get_value(v))) {
                    set_value(e.v, e.cnst->forcedLeft(get_value(v)));
                    if (!forbidden[e.v]) {
                        q.push_back(e.v);
                    }
                }
            }
        }
    }

    inline unsigned long get_n(void){
        return t.size();
    }

    int old_name(long v) {
        return old_names[v];
    }

    unsigned long degree(long v) {
        return t[v].size();
    }

    unsigned long num_vertices() {
        return t.size();
    }

    unsigned long num_edges() {
        unsigned long ret = 0;
        for (auto &v : t) {
            for (auto &e : v) {
                if (e.id > ret) {
                    ret = e.id;
                }
            }
        }
        return ret + 1;
    }

    long is_edge(long v, long u) {
        for (unsigned long p = 0; p < degree(v); ++p) {
            if (t[v][p].v == u) {
                return (long)p;
            }
        }
        return -1;
    }

    long neighbor(long v, long e_ind) {
        return t[v][e_ind].v;
    }

    long eid(long v, long e_ind) {
        return t[v][e_ind].id;
    }

    Constraint *cnst(long v, long e_ind) {
        return t[v][e_ind].cnst;
    }

    pair<long, long> rev_edge(long v, long e_ind) {
        Edge &e = t[v][e_ind];
        for (long f_ind = 0; f_ind < (long)t[e.v].size(); ++f_ind) {
            if (e.id == t[e.v][f_ind].id) {
                return {e.v, f_ind};
            }
        }
        assert(false);
    };

    long add_extra_vertex() {
        t.push_back(vector<Edge>());
        assignment.push_back(-1);
        marker.push_back(-1);
        return (long)t.size() - 1;
    }

    void pop_extra_vertex() {
        t.pop_back();
        assignment.pop_back();
        marker.pop_back();
    }

    void add_edge(long v, long u, long eid, Constraint *cnst) {
        t[v].push_back(Edge(eid, u, cnst));
        t[u].push_back(Edge(eid, v, cnst));
    }

    void remove_edge_by_index(long v, unsigned long p) {
        assert(v >= 0 && v <= t.size());
        assert(p >= 0 && p < t[v].size());
        unsigned long eid = t[v][p].id;
        long u = t[v][p].v;
        remove_dart_by_index(v, p);
        remove_dart_by_eid(u, eid);
    }

    void remove_edge_by_eid(long v, long eid) {
        assert(v >= 0 && v <= t.size());
        for (unsigned long p = 0; p < t[v].size(); ++p) {
            if (t[v][p].id == eid) {
                remove_edge_by_index(v, p);
                return;
            }
        }
        assert(false);
    }


    // Does not return isolated vertices
    vector<vector<long> > connected_components() {
        long m = ++marker_cnt;
        vector<vector<long> > res;
        for (long v = 0; v < get_n(); ++v) {
            if (marker[v] < m && degree(v) > 0) {
                res.push_back(vector<long>());
                dfs_connected_components(v, m, res.back());
            }
        }
        return res;
    }

    bool oct_reduce_cc(vector<vector<long> > &cc) {
        for (auto &vs : cc) {
            if (oct_check_consistency(vs)) {
                for (auto v : vs) {
                    vector<long> conf = put_into_undeletable(v, tmp_assignment[v]);
                    assert(conf.empty());
                }
                return true;
            }
        }
        return false;
    }

    bool oct_reduce_bridges(vector<vector<long> > &cc) {
        edges_to_remove.clear();
        for (auto &vs : cc) {
            long s = oct_get_start(vs);
            for (auto v : vs) {
                vis[v] = -1;
                low[v] = -1;
            }
            long ind = 0;
            oct_dfs_bridges(s, ind, -1);
        }
        for (auto p : edges_to_remove) {
            remove_edge_by_eid(p.first, p.second);
        }
        return !edges_to_remove.empty();
    }

    bool oct_reduce_leaf_2cc(vector<vector<long> > &cc) {
        vector<vector<long> > leaf_cc;
        bool res = false;
        for (auto &vs : cc) {
            long s = oct_get_start(vs);
            for (auto v : vs) {
                vis[v] = -1;
                low[v] = -1;
            }
            leaf_cc.clear();
            long ind = 0;
            oct_dfs_leaf_2cc(s, ind, -1, leaf_cc);
            for (auto &vcc : leaf_cc) {
                if (oct_check_consistency(vcc)) {
                    vcc.pop_back();
                    for (auto v : vcc) {
                        vector<long> conf = put_into_undeletable(v, tmp_assignment[v]);
                        res = true;
                        assert(conf.empty());
                    }
                } else {
                    long cut_vertex = vcc.back();
                    vcc.pop_back();
                    if (oct_check_consistency(vcc)) {
                        put_into_solution(cut_vertex);
                        stack->push_back(cut_vertex);
                        for (auto v : vcc) {
                            vector<long> conf = put_into_undeletable(v, tmp_assignment[v]);
                            assert(conf.empty());
                        }
                        return true;
                    }
                }
            }
        }
        return res;
    }

    bool oct_look_at_deg2_vertices() {
        vector<pair<pair<long, long>, long> > hops;
        for (long v = 0; v < get_n(); ++v) {
            if (degree(v) == 2 && assignment[v] == VAL_NONE) {
                long u = t[v][0].v;
                long w = t[v][1].v;
                if (degree(u) > degree(w)) {
                    long e = u; u = w; w = e;
                }
                long p = is_edge(u, w);
                if (p >= 0) {
                    if (degree(u) <= 3 && get_value(u) == VAL_NONE) {
                        put_into_solution(w);
                        stack->push_back(w);
                        return true;
                    }
                } else {
                    if (degree(u) == 2 && get_value(u) == VAL_NONE) {
                        long pu = 1 - is_edge(u, v);
                        long x = neighbor(u, pu);
                        long freed_eid = eid(u, pu);
                        Constraint *freed_constraint = cnst(u, pu);
                        remove_edge_by_index(u, pu);
                        remove_edge_by_index(w, is_edge(w, v));
                        assert(x != w && x != u && x != v);
                        assert(u != w);
                        if (is_edge(x, w) < 0) {
                            add_edge(x, w, freed_eid, freed_constraint);
                        }
                        t[u].clear();
                        t[v].clear();
                        set_value(u, 0);
                        set_value(v, 1);
                        return true;
                    } else {
                        hops.push_back({{u, w}, v});
                    }
                }
            }
        }
        std::sort(hops.begin(), hops.end());
        bool ret = false;
        for (unsigned long i = 1; i < hops.size(); ++i) {
            if (hops[i].first == hops[i-1].first) {
                remove_vertex(hops[i].second);
                set_value(hops[i].second, 0);
                ret = true;
            }
        }
        return ret;
    }

    void reduce_oct(vector<long> &sol_stack) {
        stack = &sol_stack;
        vis = vector<long>(t.size(), -1);
        low = vector<long>(t.size(), -1);
        tmp_assignment = assignment;
        bool reduced = true;
        while(reduced) {
            vector<vector<long> > cc = connected_components();
            reduced = oct_reduce_cc(cc);
            if (reduced) {
                continue;
            }
            reduced = oct_reduce_bridges(cc);
            if (reduced) {
                continue;
            }
            reduced = oct_reduce_leaf_2cc(cc);
            if (reduced) {
                continue;
            }
            reduced = oct_look_at_deg2_vertices();
            if (reduced) {
                continue;
            }
        }
        stack = NULL;
    }
};

#endif //CSP_GRAPH_H
