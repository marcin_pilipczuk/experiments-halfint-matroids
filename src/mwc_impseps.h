#ifndef MWC_IMPSEPS_H
#define MWC_IMPSEPS_H

#include <cstdio>
#include <vector>
#include "mwc.h"

using std::vector;
using std::pair;

struct MWCBranchingGraph {
    vector<vector<long> > t;
    vector<long> names;
    vector<long> color;
    vector<long> status;

    void resize(unsigned n) {
        t.resize(n);
        names.resize(n, -1);
        color.resize(n, -1);
        status.resize(n, 0);
    }

    void remove_vertex(long v) {
        for (auto u : t[v]) {
            for (unsigned i = 0; i < t[u].size(); ++i) {
                if (t[u][i] == v) {
                    t[u][i] = t[u].back();
                    t[u].pop_back();
                    break;
                }
            }
        }
        t[v].clear();
    }

    bool set_color(long v, long c) {
        bool ret = (color[v] >= 0 && color[v] != c);
        color[v] = c;
        return ret;
    }

    void induced_subgraph(MWCBranchingGraph &g, vector<long> &cc) {
        long n = 0;
        for (auto v : cc) {
            if (v > n) {
                n = v;
            }
        }
        n++;
        vector<long> rev(n, -1);
        for (unsigned i = 0; i < cc.size(); ++i) {
            rev[cc[i]] = i;
        }
        resize((unsigned)cc.size());
        for (unsigned i = 0; i < cc.size(); ++i) {
            long v = cc[i];
            names[i] = v;
            color[i] = g.color[v];
            status[i] = g.status[v];
            for (auto u : g.t[cc[i]]) {
               t[i].push_back(rev[u]);
            }
        }
    }
};

class MWCClassicSolver : public MWCSolver {
protected:
    MWCBranchingGraph init_g;
    vector<long> vis;
    long vis_cnt;
    vector<vector<long> > cc;
    long solution_cap;
public:
    explicit MWCClassicSolver() : vis_cnt(0), solution_cap(-1) { use_split_cc = false; }

    void make_undeletable(MWCBranchingGraph &g, long v) {
        assert(g.color[v] >= 0);
        long c = g.color[v];
        for (auto u : g.t[v]) {
            assert(g.status[u] == 0);
            if (g.set_color(u, c)) {
                current_solution.push_back(u);
                g.status[u] = 1;
                g.remove_vertex(u);
            }
        }
        g.remove_vertex(v);
        g.status[v] = -1;
    }

    void initialize() {
        read_input();
        init_g.resize(n);
        for (long v = 0; v < n; ++v) {
            init_g.names[v] = v;
        }
        vis.resize(n, 0);
        vis_cnt = 0;
        solution_cap = -1;

        for (auto e : edges) {
            init_g.t[e.second].push_back(e.first);
            init_g.t[e.first].push_back(e.second);
        }
        for (auto t : terminals) {
            init_g.set_color(t, t);
            make_undeletable(init_g, t);
        }
    }

    void cc_dfs(MWCBranchingGraph &g, vector<long> &cc, long v) {
        cc.push_back(v);
        vis[v] = vis_cnt;
        for (auto u : g.t[v]) {
            if (vis[u] != vis_cnt) {
                cc_dfs(g, cc, u);
            }
        }
    }

    unsigned long connected_components(MWCBranchingGraph &g) {
        cc.clear();
        vis_cnt++;
        unsigned long best = 0;
        for (long v = 0; v < n; ++v) {
            if (g.status[v] == 0 && vis[v] < vis_cnt) {
                cc.push_back(vector<long>());
                cc_dfs(g, cc.back(), v);
                if (cc.back().size() > cc[best].size()) {
                    best = cc.size() - 1;
                }
            }
        }
        return best;
    }

    void solve() {
        branching(init_g);
    }

    void try_update_solution() {
        MWCSolver::try_update_solution();
        if (solution_found && solution_cap > best_solution.size()) {
            solution_cap = best_solution.size();
        }
    }

    bool prune() {
        return (solution_cap >= 0 && current_solution.size() >= solution_cap);
    }

    void branching(MWCBranchingGraph &g) {
        if (!use_split_cc) {
            branching_no_cc(g);
            return;
        }
        unsigned long s = current_solution.size();
        bool unresolved_cc = false;
        long largest = connected_components(g);
        for (unsigned i = 0; i < cc.size(); ++i) {
            if (i != largest) {
                MWCBranchingGraph comp;
                comp.induced_subgraph(g, cc[i]);
                MWCClassicSolver solver;
                solver.use_split_cc = true;
                solver.solution_cap = (solution_cap >= 0 ? solution_cap - current_solution.size() : -1);
                solver.n = cc[i].size();
                solver.vis.resize(cc[i].size(), 0);
                solver.vis_cnt = 0;
                solver.branching_no_cc(comp);
                if (!solver.solution_found) {
                    unresolved_cc = true;
                    break;
                }
                for (auto v : cc[i]) {
                    g.remove_vertex(v);
                    g.status[v] = -1;
                }
                for (auto v : solver.best_solution) {
                    assert(g.status[comp.names[v]] == -1);
                    current_solution.push_back(comp.names[v]);
                    g.status[comp.names[v]] = 1;
                    g.remove_vertex(comp.names[v]);
                }
            }
        }
        if (!unresolved_cc) {
            if (largest < 0 || largest >= cc.size() || cc[largest].empty()) {
                try_update_solution();
            } else if (!prune()) {
                branching_no_cc(g);
            }
        }
        while (current_solution.size() > s) {
            current_solution.pop_back();
        }
    }

    long maxflow(MWCBranchingGraph &g, vector<long> &X, vector<long> &Y, vector<long> &cut) {
        unsigned long n = g.t.size();
        vector<long> queue, next(n, -1), prev(n, -1), isY(n, 0), step(n), special(n);
        for (auto u : Y) {
            isY[u] = 1;
        }
        bool augmentation_found;
        long iter_cnt = 0;
        do{
            augmentation_found = false;
            vis_cnt++;
            queue.clear();
            for (auto v : X) {
                if (next[v] < 0) {
                    queue.push_back(v);
                    vis[v] = vis_cnt;
                    step[v] = -1;
                }
            }
            while (!augmentation_found && !queue.empty()) {
                long v = queue.back();
                queue.pop_back();
                for (auto u : g.t[v]) {
                    if (u != next[v]) {
                        if (prev[u] >= 0 || next[u] >= 0) {
                            long x = u, y = prev[u];
                            bool first = true;
                            while (y >= 0 && vis[y] < vis_cnt) {
                                vis[y] = vis_cnt;
                                queue.push_back(y);
                                if (first) {
                                    step[y] = v;
                                    special[y] = x;
                                    first = false;
                                } else {
                                    step[y] = x;
                                    special[y] = -1;
                                }
                                x = y;
                                y = prev[y];
                            }
                        } else {
                            if (isY[u]) {
                                step[u] = v;
                                special[u] = -1;
                                long x = u;
                                while (step[x] >= 0) {
                                   long y = step[x];
                                   if (special[x] >= 0) {
                                       long z = special[x];
                                       prev[z] = y;
                                       next[y] = z;
                                   } else if (prev[y] == x) {
                                       prev[y] = -1;
                                       next[y] = -1;
                                   } else {
                                       next[y] = x;
                                       prev[x] = y;
                                   }
                                   x = y;
                                }
                                augmentation_found = true;
                                iter_cnt++;
                                break;
                            } else if (vis[u] < vis_cnt){
                                vis[u] = vis_cnt;
                                step[u] = v;
                                special[u] = -1;
                                queue.push_back(u);
                            }
                        }
                    }
                }
            }
        }while(augmentation_found);
        queue.clear();
        long c = g.color[Y.back()];
        assert(c >= 0);
        cut.clear();
        queue = Y;
        for (auto v : Y) {
            assert(g.status[v] == 0);
            assert(g.color[v] == c);
            assert(vis[v] < vis_cnt);
            vis[v] = vis_cnt + 1;
        }
        long deleted_cnt = 0;
        while (!queue.empty()) {
            long v = queue.back();
            queue.pop_back();
            bool lonely = g.color[v] < 0 || g.color[v] == c;
            for (auto u : g.t[v]) {
                assert(g.status[u] == 0);
                if (vis[u] == vis_cnt) {
                    lonely = false;
                } else if (vis[u] < vis_cnt) {
                    vis[u] = vis_cnt + 1;
                    queue.push_back(u);
                }
            }
            if (g.set_color(v, c)) {
                assert(g.status[v] == 0);
                current_solution.push_back(v);
                g.status[v] = 1;
                g.remove_vertex(v);
                deleted_cnt++;
            } else if (lonely) {
                assert(g.status[v] == 0);
                g.status[v] = -1;
                g.remove_vertex(v);
            } else {
                assert(g.status[v] == 0);
                cut.push_back(v);
            }
        }
        long pivot = -1;
        for (auto v : cut) {
            if (pivot < 0 || g.t[pivot].size() < g.t[v].size()) {
                pivot = v;
            }
        }
        assert(iter_cnt == deleted_cnt + cut.size());
        vis_cnt++;
        return pivot;
    }

    void branching_no_cc(MWCBranchingGraph &g) {
        if (prune()) {
            return;
        }
        vis_cnt++;
        vector<long> seen_colors;
        for (long v = 0; v < g.t.size(); ++v) {
            if (g.status[v] == 0) {
                if (g.color[v] >= 0) {
                    bool seen = false;
                    for (auto c : seen_colors) {
                        if (c == g.color[v]) {
                            seen = true;
                            break;
                        }
                    }
                    if (!seen) {
                        seen_colors.push_back(g.color[v]);
                        if (seen_colors.size() >= 3) {
                            break;
                        }
                    }
                }
            }
        }
        if (seen_colors.size() <= 1) {
            try_update_solution();
            return;
        }
        long one_color = seen_colors.front();
        vector<long> X, Y;
        for (long v = 0; v < g.t.size(); ++v) {
            if (g.status[v] == 0 && g.color[v] >= 0) {
                if (g.color[v] == one_color) {
                    Y.push_back(v);
                } else {
                    X.push_back(v);
                }
            }
        }
        unsigned long s = current_solution.size();
        vector<long> cut;
        long pivot = maxflow(g, X, Y, cut);
        if (seen_colors.size() == 2) {
            for (auto v : cut) {
                assert(g.status[v] == 0);
                assert(g.color[v] == one_color);
                current_solution.push_back(v);
                g.remove_vertex(v);
                g.status[v] = 1;
            }
            try_update_solution();
        } else if (pivot < 0) {
            // Means MaxFlow deleted all boundary.
            assert(cut.empty());
            branching(g);
        } else if (!prune()) {
            MWCBranchingGraph copy = g;
            assert(g.color[pivot] == one_color);

            current_solution.push_back(pivot);
            g.status[pivot] = 1;
            g.remove_vertex(pivot);
            branching(g);
            current_solution.pop_back();

            make_undeletable(copy, pivot);
            branching(copy);
        }
        while (current_solution.size() > s) {
            current_solution.pop_back();
        }
    }
};

#endif
