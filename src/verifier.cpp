/*
 Copyright 2016 Marcin Pilipczuk.

 This file is part of fvs_pace_challenge,
 an implementation of FPT algorithm for Feedback Vertex Set,
 a submission to track B of PACE Challenge 2016.

 fvs_pace_challenge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 fvs_pace_challenge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with fvs_pace_challenge.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>

using namespace std;
unordered_map<string, int> names;
vector<int> fau;

int name2int(char *name){
    string str(name);
    if (names.count(str))
        return names[str];
    int id = (int)fau.size();
    fau.push_back(-1);
    names[str] = id;
    return id;
}

int fau_find(int a){
    return fau[a] < 0 ? a : fau[a] = fau_find(fau[a]);
}

bool fau_union(int a, int b){
    a = fau_find(a);
    b = fau_find(b);
    if (a == b) return false;
    if (fau[a] > fau[b]) swap(a,b);
    else if (fau[a] == fau[b]) fau[a]--;
    fau[b] = a;
    return true;
}

int main(int argc, char **argv){
    char line[10000], name1[10000], name2[10000];
    FILE *f;
    int opt = -1;
    if (argc > 3){
        // There is an optimal solution provided
        f = fopen(argv[3], "r");
        opt = 0;
        while (fgets(line, 9999, f)) opt++;
        fclose(f);
    }
    f = fopen(argv[2], "r");
    int solcnt = 0;
    fscanf(f, "%d", &solcnt);
    for (int i = 0; i < solcnt; ++i) {
      fscanf(f, "%s", line);
      names[string(line)] = -1;
    }
    fclose(f);
    f = fopen(argv[1], "r");
    while(fgets(line, 9999, f)) {
        int i = 0;
        while (line[i] == ' ' || line[i] == '\t' || line[i] == '\n' || line[i] == '\r') ++i;
        if (line[i] == 0 || line[i] == '#') continue;
        sscanf(line, "%s %s", name1, name2);
        int a = name2int(name1);
        int b = name2int(name2);
        if (a >=0 && b >= 0)
            if (!fau_union(a, b))
                return -1;
    }
    fclose(f);
    printf("solution size = %4d", solcnt);
    if (opt >= 0){
        printf (", opt = %4d ", opt);
        if (opt < solcnt)
            return 1;
        else if (opt == solcnt)
            return 0;
        else
            return 2;
    } else {
        printf(", opt unknown");
        return 0;
    }
}

