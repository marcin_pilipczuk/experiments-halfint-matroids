//
// Created by malcin on 03.07.18.
//

#ifndef CSPSOLVER_SOLVER_H
#define CSPSOLVER_SOLVER_H

#include "values.h"
#include "csp_graph.h"

using std::pair;

//#define DEBUG_PRINTS 1

struct CSPSolution {
    vector<long> halves;
    vector<long> ones;
    long lower_bound;
};

enum VtxType {
    VtxO, VtxI, VtxT, VtxH, VtxS, VtxA
};

enum EdgeType {
    EdgeO, EdgeI, EdgeH, EdgeS
};

struct ForcingPath {
    vector<long> v;
    vector<long> e_ind_f, e_ind_r;
    vector<value_t> val_f, val_r;
};

enum DFSResult {
    NotUsed, SpokeForward, SpokeBackward, Path, Cycle
};

class Solver {
private:
    CSPGraph &g;
    unsigned long n, m;
    Instance &inst;
    vector<value_t> vals;
    vector<value_t> initial_vals;
    vector<long> prev_v, escape_prev_v;
    vector<long> prev_e, escape_prev_e;
    vector<VtxType> vtx_types;
    vector<EdgeType> edge_types;
    vector<ForcingPath> integral_paths;
    vector<ForcingPath> spokes;
    vector<ForcingPath> cycles;
    vector<long> path_pos, path_ind;
    vector<long> cycle_pos, cycle_ind;
    vector<long> vis_f, vis_r, vis_spokes;
    vector<long> marker, marker_data;
    long marker_cnt;

    void rewind_search(vector<long> &visited, vector<pair<long, long> > &chg_vis_f,
                       vector<pair<long, long> > &chg_vis_r, vector<pair<long, long> > &chg_vis_spokes);
    bool augment(long start=-1);
    vector<pair<long, long> > unwind_path(long v, long e_ind);
    vector<pair<long, long> > unwind_path(long v);
    void xor_path(long v, long e_ind);
    void xor_path(vector<pair<long, long> > &P);
    void xor_edge(long v, long e_ind);
    void break_cycle(long v, EdgeType t);
    value_t value_one_step(long v, long eid, value_t a);
    bool juggle_pair(vector<pair<long, long> > &Pv, vector<pair<long, long> > &Pu);
    DFSResult recompute_dfs(long v, long eid, bool seen_T);
    void recompute_packing();
    void augment_hit_A(long v, long e_ind);
    void augment_hit_spoke(long v, long e_ind);
    void augment_hit_spoke(vector<pair<long, long> > &P);
    void augment_hit_cycle(long v, long e_ind, value_t b);
    void augment_pair(long v, long e_ind);
    void farthest(void);
    CSPSolution packing2hitting();

    void debug_print_chosen_edges();
    void debug_print_path(vector<pair<long, long> > P);
public:
    Solver(CSPGraph &_g, Instance &_inst) : g(_g), inst(_inst) { marker_cnt = 0; };
    CSPSolution solve();
};

#endif //CSPSOLVER_SOLVER_H
