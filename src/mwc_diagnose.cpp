#include "mwc.h"

int main(void) {
    CSPMWCSolver s;
    s.initialize();
    pair<long, long> lb = s.lower_bounds();
    printf("%ld %ld %ld %ld %ld\n", s.get_n(), s.get_m(), s.get_t(), lb.first, lb.second);
    return 0;
}