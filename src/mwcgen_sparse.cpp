// Test generator for Multiway cut from sparse graphs

#include<cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <random>
#include <map>
#include <iostream>
#include <set>

using std::cin;
using std::vector;
using std::pair;
using std::swap;
using std::map;
using std::string;
using std::set;

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: mwcgen_sparse terms\n");
        return -1;
    }

    int t = atoi(argv[1]);
    std::random_device rand_dev;
    std::default_random_engine re(rand_dev());

    // Read input
    map<string, int> names;
    vector<vector<int> > g;
    string a, b;
    while (cin >> a) {
        cin >> b;
        if (names.count(a) == 0) {
            names[a] = (int)g.size();
            g.push_back(vector<int>());
        }
        if (names.count(b) == 0) {
            names[b] = (int)g.size();
            g.push_back(vector<int>());
        }
        g[names[a]].push_back(names[b]);
        g[names[b]].push_back(names[a]);
    }
    int n = g.size();
    vector<int> status(n, 0);
    vector<int> remap(n), terms;
    vector<int> perm(n);
    for (int i = 0; i < n; ++i) {
        perm[i] = i;
        remap[i] = i;
        int j = std::uniform_int_distribution<int>(0, i)(re);
        swap(perm[i], perm[j]);
    }
    while (t <= 0 || terms.size() < t) {
        int v;
        for (v = 0; v < n; ++v) {
            if (status[perm[v]] == 0) {
                break;
            }
        }
        if (v == n) {
            break;
        }
        v = perm[v];
        terms.push_back(v);
        for (auto w : g[v]) {
            remap[w] = v;
        }
        vector<int> dist(n, -1), q1, q2;
        dist[v] = 0; q1.push_back(v);
        status[v] = 1;
        for (int d = 1; d <= 4; ++d) {
            for (auto w : q1) {
                for (auto u : g[w]) {
                    if (dist[u] == -1) {
                        dist[u] = d;
                        status[u] = 2;
                        q2.push_back(u);
                    }
                }
            }
            q1.clear();
            swap(q1, q2);
        }
    }

    map<int, int> final_names;
    int n_final = 0;
    for (int v = 0; v < n; ++v) {
        if (remap[v] == v) {
            final_names[remap[v]] = n_final++;
        }
    }
    perm.resize(n_final);
    for (int i = 0; i < n_final; ++i) {
        perm[i] = i;
        int j = std::uniform_int_distribution<int>(0, i)(re);
        swap(perm[i], perm[j]);
    }
    for (int v = 0; v < n; ++v) {
        if (remap[v] == v) {
            final_names[v] = perm[final_names[v]];
        }
    }
    set<pair<int, int> > edges;
    for (int v = 0; v < n; ++v) {
        for (auto u : g[v]) {
            int a = final_names[remap[v]];
            int b = final_names[remap[u]];
            if (a > b) {
                swap(a, b);
            }
            if (a != b) {
                edges.insert({a, b});
            }
        }
    }
    printf("%d %lu %lu\n", n_final, edges.size(), terms.size());
    for (auto t : terms) {
        printf("%d ", final_names[t]);
    }
    printf("\n");
    for (auto e : edges) {
        printf("%d %d\n", e.first, e.second);
    }
    return 0;
}
