//
// Created by malcin on 19.07.18.
//

#include <cstdio>
#include <vector>

using std::vector;
using std::pair;

vector<pair<int, int> > edges;
int num_verts;

int maxcut(void) {
    int best = 0;
    for (unsigned long x = 0; x < (1lu << num_verts); ++x) {
        int cnt = 0;
        for (auto p : edges) {
            if (((x >> p.first) & 1lu) != ((x >> p.second) & 1lu)) {
                cnt++;
            }
        }
        if (cnt > best) {
            best = cnt;
        }
    }
    return best;
}

void n_connection(FILE *f, int pos, int t1, int t2, int n) {
    for (int a = 0; a < n; ++a) {
        fprintf(f, "%d %d\n", t1, pos + a);
        fprintf(f, "%d %d\n", t2, pos + a + n);
        for (int b = 0; b < n; ++b) {
            fprintf(f, "%d %d\n", pos + a, pos + b + n);
            if (a < b) {
                fprintf(f, "%d %d\n", pos + a, pos + b);
                fprintf(f, "%d %d\n", pos + a + n, pos + b + n);
            }
        }
    }
}

void four_connection(FILE *f, int pos, int t1, int t2) {
  n_connection(f, pos, t1, t2, 4);
}

int main(int argc, char *argv[]) {
    FILE *f;

    f = fopen(argv[1], "r");
    int a, b, m;
    fscanf(f, "%d%d", &num_verts, &m);
    vector<int> degrees(num_verts, 0);
    vector<vector<int> > cpos(num_verts);
    while(m--) {
        fscanf(f, "%d%d", &a, &b);
        edges.push_back({a, b});
        degrees[a]++;
        degrees[b]++;
    }
    fclose(f);

    f = fopen(argv[2], "w");
    int new_n = 3 + (8*6 + 6)*(int)edges.size();
    int new_m = (int)edges.size() * (6 * (8 + 8 * 7 / 2 + 16));
    for (int i = 0; i < num_verts; ++i) {
      new_m += degrees[i] * (degrees[i] - 1) * 32;
    }
    fprintf(f, "%d %d 3\n0 1 2\n", new_n, new_m);
    int pos = 3;
    for (auto p : edges) {
        four_connection(f, pos, 0, 1);
        four_connection(f, pos + 8, 0, 1);
        four_connection(f, pos + 16, 0, 2);
        four_connection(f, pos + 24, 0, 2);
        four_connection(f, pos + 32, 1, 2);
        four_connection(f, pos + 40, 1, 2);
        cpos[p.first].push_back(pos);
        cpos[p.second].push_back(pos + 8);
        int pos2 = pos + 48;
        for (int a = 0; a < 8; ++a) {
            fprintf(f, "%d %d\n", pos + 40 + a, pos2 + 0);
            fprintf(f, "%d %d\n", pos + 0 + a, pos2 + 0);
            fprintf(f, "%d %d\n", pos + 0 + a, pos2 + 1);
            fprintf(f, "%d %d\n", pos + 16 + a, pos2 + 1);
            fprintf(f, "%d %d\n", pos + 16 + a, pos2 + 2);
            fprintf(f, "%d %d\n", pos + 32 + a, pos2 + 2);
            fprintf(f, "%d %d\n", pos + 32 + a, pos2 + 3);
            fprintf(f, "%d %d\n", pos + 8 + a, pos2 + 3);
            fprintf(f, "%d %d\n", pos + 8 + a, pos2 + 4);
            fprintf(f, "%d %d\n", pos + 24 + a, pos2 + 4);
            fprintf(f, "%d %d\n", pos + 24 + a, pos2 + 5);
            fprintf(f, "%d %d\n", pos + 40 + a, pos2 + 5);
        }
        pos += 48 + 6;
    }
    for (int i = 0; i < num_verts; ++i) {
      for (int a = 0; a < degrees[i]; ++a) {
        for (int b = 0; b < a; ++b) {
          for (int p = 0; p < 8; ++p) {
            for (int q = 0; q < 8; ++q) {
              fprintf(f, "%d %d\n", cpos[i][b] + p, cpos[i][a] +  q);
            }
          }
        }
      }
    }
    fclose(f);
    if (pos != new_n) {
        fprintf(stderr, " yyyyy... wrong number of vertices? %d vs %d\n", new_n, pos);
    }

    int maxcut_opt = maxcut();
    int mwc_opt = 28 * (int)edges.size() - maxcut_opt;
    f = fopen(argv[3], "w");
    fprintf(f, "%d\n", mwc_opt);
    fclose(f);

    fprintf(stderr, "   Input n=%3d, m=%3d, opt=%3d, Output n=%4d, m=%4d, opt=%4d\n", num_verts, (int)edges.size(), maxcut_opt, new_n, new_m, mwc_opt);

    return 0;
}
