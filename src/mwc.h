//
// Created by malcin on 20.07.18.
//

#ifndef CSPSOLVER_MWC_H
#define CSPSOLVER_MWC_H

#include <cstdio>
#include <vector>
#include <set>
#include "csp_graph.h"
#include "solver.h"

using std::vector;
using std::pair;


class MWCSolver {
protected:
    vector<long> best_solution, current_solution;
    bool solution_found;

    vector<pair<long, long> > edges;
    vector<long> terminals;
    long n;

    void try_update_solution() {
        if (!solution_found || current_solution.size() < best_solution.size()) {
            best_solution = current_solution;
            solution_found = true;
        }
    }

public:
    bool use_split_cc;


    MWCSolver() { solution_found = false; };
    virtual ~MWCSolver() {};

    void read_input() {
        long m, a, b, t;
        scanf("%ld%ld%ld", &n, &m, &t);
        while(t--) {
            scanf("%ld", &a);
            terminals.push_back(a);
        }
        while(m--) {
            scanf("%ld%ld", &a, &b);
            edges.push_back({a, b});
        }
    }

    void print_solution(void) {
        printf("%lu\n", best_solution.size());
        for (auto a : best_solution) {
            printf("%ld ", a);
        }
        printf("\n");
    }

    long get_n() {
        return n;
    }

    long get_m() {
        return (long)edges.size();
    }

    long get_t() {
        return (long)terminals.size();
    }

    virtual void initialize() = 0;
    virtual void solve() = 0;
};

struct MWCPreprocessingOutput {
   std::set<pair<long, long> > edges;
   vector<long> terminals;
   long n = 0;

   vector<long> undel_vertices;
   std::set<long> iso_terminals;
   vector<long> deleted;
   long total_terminals_degree = 0;

   void output_graph() {
       printf("%ld %ld %ld\n", n, edges.size(), terminals.size());
       for (auto t : terminals) {
           printf("%ld ", t);
       }
       printf("\n");
       for (auto e : edges) {
           printf("%ld %ld\n", e.first, e.second);
       }
   }

   void output_report() {
       fprintf(stderr, "DELETED %lu\n", deleted.size());
       for (auto v : deleted) {
           fprintf(stderr, "%ld ", v);
       }
       fprintf(stderr, "\nUNDELETABLE_VERTICES %lu\n", undel_vertices.size());
       for (auto v : undel_vertices) {
           fprintf(stderr, "%ld ", v);
       }
       fprintf(stderr, "\nRESOLVED_TERMINALS %lu\n", iso_terminals.size());
       for (auto v : iso_terminals) {
           fprintf(stderr, "%ld ", v);
       }
       fprintf(stderr,"\nTOTAL_TERMINALS_DEGREE %ld\n", total_terminals_degree);
   }
};

class CSPMWCSolver : public MWCSolver {
protected:
    CSPGraph g;
    long solution_cap;
public:
    explicit CSPMWCSolver() { use_split_cc = false; solution_found = false; solution_cap = -1; };
    virtual ~CSPMWCSolver() { }

    virtual void initialize() {
        read_input();
        current_solution = g.make_mwc_instance(n, edges, terminals);
        if (use_split_cc) {
            g.initialize_marker();
        }
    }

    virtual CSPMWCSolver *clone() {
       return new CSPMWCSolver();
    }

    pair<long, long> lower_bounds() {
        Instance inst = g.generate_instance();
        Solver solver(g, inst);
        CSPSolution s = solver.solve();
        return {(long)(current_solution.size() + s.ones.size() + (s.halves.size() + 1)/2), current_solution.size() + s.lower_bound};
    }

    MWCPreprocessingOutput preprocess() {
        Instance inst = g.generate_instance();
        Solver solver(g, inst);
        CSPSolution s = solver.solve();

        vector<long> forbidden(g.get_n(), 0);
        for (auto a : current_solution) {
            forbidden[a] = 2;
        }
        for (auto a : s.halves) {
            forbidden[a] = 1;
        }
        for (auto a : s.ones) {
            forbidden[a] = 2;
            current_solution.push_back(a);
            g.put_into_solution(a);
        }
        g.flood_zeroes(forbidden);

        MWCPreprocessingOutput po;
        po.n = g.get_n();
        po.terminals = terminals;
        vector<int> degrees(g.get_n(), 0);
        for (auto e :  edges) {
            if (forbidden[e.first] == 2 || forbidden[e.second] == 2) {
                continue;
            }
            if (g.get_value(e.first) >= 0 && !forbidden[e.first]) {
                e.first = terminals[g.get_value(e.first)];
            }
            if (g.get_value(e.second) >= 0 && !forbidden[e.second]) {
                e.second = terminals[g.get_value(e.second)];
            }
            if (e.first > e.second) {
                std::swap(e.first, e.second);
            }
            if (e.first != e.second) {
                po.edges.insert(e);
            }
        }
        for (auto e : po.edges) {
            degrees[e.first]++;
            degrees[e.second]++;
        }
        po.total_terminals_degree = 0;
        for (auto t : terminals) {
            if (degrees[t] == 0) {
                po.iso_terminals.insert(t);
            } else {
                po.total_terminals_degree += degrees[t];
            }
        }
        for (long v = 0; v < g.get_n(); ++v) {
            if (g.get_value(v) >= 0 && !po.iso_terminals.count(v) && !forbidden[v]) {
                assert(degrees[v] == 0);
                po.undel_vertices.push_back(v);
            }
        }
        po.deleted = current_solution;
        sort(po.deleted.begin(), po.deleted.end());
        return po;
    }

    virtual void solve() {
        branching(g);
    }

    long final_cap() {
        long res = solution_cap;
        if (solution_found && (res < 0 || res > best_solution.size())) {
            res = best_solution.size();
        }
        return res;
    }

    bool lower_bound_prunning(CSPSolution &s) {
        long lb = current_lower_bound(s);
        return (solution_found && lb >= best_solution.size()) || (solution_cap >= 0 && lb >= solution_cap);
    }

    /* No advanced lower bound prunning: only count vertices that will be shortly put into solution */
    virtual long current_lower_bound(CSPSolution &s) {
        return current_solution.size() + s.ones.size();
    }

    bool split_cc(CSPGraph &curr_g) {
        auto ccs = curr_g.connected_components();
        unsigned long largest = 0;
        for (unsigned long i = 0; i < ccs.size(); ++i) {
            if (ccs[i].size() > ccs[largest].size()) {
                largest = i;
            }
        }
        for (unsigned long i = 0; i < ccs.size(); ++i) {
            if (i != largest) {
                CSPMWCSolver *solver = this->clone();
                solver->use_split_cc = use_split_cc;
                CSPGraph cc(curr_g, ccs[i]);
                solver->solution_cap = final_cap();
                if (solver->solution_cap >= 0) {
                    solver->solution_cap -= current_solution.size();
                }
                solver->branching(cc);
                if (!solver->solution_found) {
                    delete solver;
                    return false;
                }
                for (auto v : solver->best_solution) {
                    curr_g.put_into_solution(cc.old_name(v));
                    current_solution.push_back(cc.old_name(v));
                }
                delete solver;
                if ((solution_found && current_solution.size() >= best_solution.size()) || (solution_cap >= 0 && current_solution.size() >= solution_cap)) {
                    return false;
                }
            }
        }
        return true;
    }

    void branching(CSPGraph &curr_g) {
        unsigned long sol_size = current_solution.size();
        if ((solution_found && current_solution.size() >= best_solution.size()) || (solution_cap >= 0 && current_solution.size() >= solution_cap)) {
            return;
        }
        if (!use_split_cc || split_cc(curr_g)) {
            Instance inst = curr_g.generate_instance();
            Solver solver(curr_g, inst);
            CSPSolution s = solver.solve();

            if (!lower_bound_prunning(s)) {
                vector<long> forbidden(curr_g.get_n(), 0);
                for (auto a : s.halves) {
                    forbidden[a] = 1;
                }
                for (auto a : s.ones) {
                    forbidden[a] = 1;
                    current_solution.push_back(a);
                    curr_g.put_into_solution(a);
                }
                curr_g.flood_zeroes(forbidden);

                if (s.halves.empty()) {
                    // Solved!
                    try_update_solution();
                } else {

                    long pivot = s.halves.back();

                    assert(pivot >= 0 && pivot < curr_g.get_n());
                    assert(curr_g.get_value(pivot) >= 0);

                    CSPGraph copy(curr_g);

                    curr_g.put_into_solution(pivot);
                    current_solution.push_back(pivot);
                    branching(curr_g);
                    current_solution.pop_back();

                    vector<long> conflicting = copy.put_into_undeletable(pivot, copy.get_value(pivot));
                    for (auto a : conflicting) {
                        current_solution.push_back(a);
                        copy.put_into_solution(a);
                    }
                    branching(copy);
                }
            }
        }

        while(current_solution.size() > sol_size) {
            current_solution.pop_back();
        }
    }
};

class CSPMWCSolverWithPreprocessing : public CSPMWCSolver {
public:
    virtual void solve() {
        preprocess();
        branching(g);
    }
    virtual CSPMWCSolver *clone() {
        return new CSPMWCSolverWithPreprocessing();
    }
};

/* Lower bound prunning using the lower bound directly from the weight of the CSP solution */
class CSPMWCSolverLB1 : public CSPMWCSolver {
public:
    virtual long current_lower_bound(CSPSolution &s) {
        return current_solution.size() + s.ones.size() + (s.halves.size() + 1) / 2;
    }
    virtual CSPMWCSolver *clone() {
        return new CSPMWCSolverLB1();
    }
};

/* Lower bound prunning using Iwata's improved lower bound: every cycle in the half-integral packing requires
 * ceil(#spokes / 2) vertices */
class CSPMWCSolverLB2 : public CSPMWCSolver {
public:
    virtual long current_lower_bound(CSPSolution &s) {
        return current_solution.size() + s.lower_bound;
    }
    virtual CSPMWCSolver *clone() {
        return new CSPMWCSolverLB2();
    }
};


#endif //CSPSOLVER_MWC_H
