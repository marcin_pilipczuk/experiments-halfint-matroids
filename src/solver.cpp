//
// Created by malcin on 03.07.18.
//

#include <vector>
#include <algorithm>
#include "solver.h"

using std::vector;
using std::reverse;


void Solver::rewind_search(vector<long> &visited, vector<pair<long, long> > &chg_vis_f,
                           vector<pair<long, long> > &chg_vis_r, vector<pair<long, long> > &chg_vis_spokes) {
    for (auto v : visited) {
        vals[v] = -1;
        escape_prev_v[v] = prev_v[v] = -1;
        escape_prev_e[v] = prev_e[v] = -1;
    }
    while (!chg_vis_f.empty()) {
        vis_f[chg_vis_f.back().first] = chg_vis_f.back().second;
        chg_vis_f.pop_back();
    }
    while (!chg_vis_r.empty()) {
        vis_r[chg_vis_r.back().first] = chg_vis_r.back().second;
        chg_vis_r.pop_back();
    }
    while (!chg_vis_spokes.empty()) {
        vis_spokes[chg_vis_spokes.back().first] = chg_vis_spokes.back().second;
        chg_vis_spokes.pop_back();
    }
}

bool Solver::augment(long start) {
    bool only_try = (start >= 0);
    if (!only_try) {
        // Initialize
        for (unsigned long v = 0; v < n; ++v) {
            vals[v] = -1;
            escape_prev_v[v] = prev_v[v] = -1;
            escape_prev_e[v] = prev_e[v] = -1;
        }
        vis_f = vector<long>(integral_paths.size(), 0);
        vis_r = vector<long>(integral_paths.size());
        vis_spokes = vector<long>(spokes.size(), 0);
        for (unsigned long i = 0; i < integral_paths.size(); ++i) {
            vis_r[i] = integral_paths[i].e_ind_f.size();
        }
    }
    vector<long> to_rewind;
    vector<pair<long, long> > chg_vis_f, chg_vis_r, chg_vis_spokes;
    // Initialize queue
    vector<long> q, starting;
    if (only_try) {
        starting.push_back(start);
        vals[start] = -1;
    } else {
        for (auto p : inst.init_vals) {
            if (vtx_types[p.first] == VtxType::VtxA) {
                starting.push_back(p.first);
            }
        }
    }
    for (auto s : starting) {
        if (vals[s] >= 0) {
            continue;
        }
        vals[s] = initial_vals[s];
        if (only_try) {
            to_rewind.push_back(s);
        }
        q.push_back(s);
        // Main search loop
        while (!q.empty()) {
            long v = q.back();
            q.pop_back();
            for (unsigned long e_ind = 0; e_ind < g.degree(v); ++e_ind) {
                if (edge_types[g.eid(v, e_ind)] == EdgeType::EdgeO && g.cnst(v, e_ind)->isForcedLeft(vals[v])) {
                    long u = g.neighbor(v, e_ind);
                    value_t b = g.cnst(v, e_ind)->forcedLeft(vals[v]);
                    // Case 1: u is already visited
                    if (vals[u] >= 0) {
                        if (vals[u] != b) {
                            if (only_try) {
                                rewind_search(to_rewind, chg_vis_f, chg_vis_r, chg_vis_spokes);
                            } else {
                                augment_pair(v, e_ind);
                            }
                            return true;
                        } else {
                            // Nothing happens here
                            continue;
                        }
                    }
                    // Henceforth u is not visited yet
                    // Case 2: hitting a visited path
                    if (vtx_types[u] == VtxType::VtxI) {
                        assert(path_pos[u] >= 0 && path_ind[u] >= 0);
                        long ind = path_ind[u], i = path_pos[u];
                        if (b != integral_paths[ind].val_r[i]) {
                            for (long j = i - 1; j >= vis_f[ind]; --j) {
                                long x = integral_paths[ind].v[j];
                                prev_v[x] = integral_paths[ind].v[j + 1];
                                prev_e[x] = integral_paths[ind].e_ind_r[j];
                                vals[x] = integral_paths[ind].val_f[j];
                                q.push_back(x);
                                if (only_try) {
                                    to_rewind.push_back(x);
                                }
                            }
                            if (i - 1 >= vis_f[ind]) {
                                long x = integral_paths[ind].v[i - 1];
                                escape_prev_v[x] = v;
                                escape_prev_e[x] = e_ind;
                            }
                            if (only_try) {
                                chg_vis_f.push_back({ind, vis_f[ind]});
                            }
                            vis_f[ind] = i;
                        }
                        if (b != integral_paths[ind].val_f[i]) {
                            for (long j = i + 1; j <= vis_r[ind]; ++j) {
                                long x = integral_paths[ind].v[j];
                                prev_v[x] = integral_paths[ind].v[j - 1];
                                prev_e[x] = integral_paths[ind].e_ind_f[j - 1];
                                vals[x] = integral_paths[ind].val_r[j];
                                q.push_back(x);
                                if (only_try) {
                                    to_rewind.push_back(x);
                                }
                            }
                            if (i + 1 <= vis_r[ind]) {
                                long x = integral_paths[ind].v[i + 1];
                                escape_prev_v[x] = v;
                                escape_prev_e[x] = e_ind;
                            }
                            if (only_try) {
                                chg_vis_r.push_back({ind, vis_r[ind]});
                            }
                            vis_r[ind] = i;
                        }
                        continue;
                    }
                    // Case 3: hitting a spoke
                    if (vtx_types[u] == VtxType::VtxS || vtx_types[u] == VtxType::VtxT) {
                        assert(path_pos[u] >= 0 && path_ind[u] >= 0);
                        long ind = path_ind[u], i = path_pos[u];
                        if (spokes[ind].val_f[i] != b) {
                            if (only_try) {
                                rewind_search(to_rewind, chg_vis_f, chg_vis_r, chg_vis_spokes);
                            } else {
                                augment_hit_spoke(v, e_ind);
                            }
                            return true;
                        }
                        if (vis_spokes[ind] >= i) {
                            continue;
                        }
                        // vals[u] = b; (???)
                        for (long j = i - 1; j >= vis_spokes[ind]; --j) {
                            long x = spokes[ind].v[j];
                            prev_v[x] = spokes[ind].v[j + 1];
                            prev_e[x] = spokes[ind].e_ind_r[j];
                            vals[x] = spokes[ind].val_f[j];
                            q.push_back(x);
                            if (only_try) {
                                to_rewind.push_back(x);
                            }
                        }
                        if (i - 1 >= vis_spokes[ind]) {
                            long x = spokes[ind].v[i - 1];
                            escape_prev_v[x] = v;
                            escape_prev_e[x] = e_ind;
                        }
                        if (only_try) {
                            chg_vis_spokes.push_back({ind, vis_spokes[ind]});
                        }
                        vis_spokes[ind] = i;
                        continue;
                    }
                    // Case 4: hitting a cycle
                    if (vtx_types[u] == VtxType::VtxH) {
                        if (only_try) {
                            rewind_search(to_rewind, chg_vis_f, chg_vis_r, chg_vis_spokes);
                        } else {
                            augment_hit_cycle(v, e_ind, b);
                        }
                        return true;
                    }
                    // Case 5: hitting A
                    if (vtx_types[u] == VtxType::VtxA) {
                        if (b != initial_vals[u]) {
                            if (only_try) {
                                rewind_search(to_rewind, chg_vis_f, chg_vis_r, chg_vis_spokes);
                            } else {
                                augment_hit_A(v, e_ind);
                            }
                            return true;
                        }
                        continue; // nothing else
                    }
                    // Case 6: normal expansion
                    assert(vtx_types[u] == VtxType::VtxO);
                    prev_v[u] = v;
                    prev_e[u] = e_ind;
                    vals[u] = b;
                    q.push_back(u);
                    if (only_try) {
                        to_rewind.push_back(u);
                    }
                }
            }
        }
    }
    // No rewinding here
    return false;
}

void Solver::xor_path(long v, long e_ind) {
    vector<pair<long, long> > P = unwind_path(v, e_ind);
    xor_path(P);
}

void Solver::xor_path(vector<pair<long, long> > &P) {
    for (auto p : P) {
        xor_edge(p.first, p.second);
    }
}

void Solver::xor_edge(long v, long e_ind) {
    long eid = g.eid(v, e_ind);
    if (edge_types[eid] == EdgeType::EdgeO) {
        edge_types[eid] = EdgeType::EdgeI;
    } else {
        edge_types[eid] = EdgeType::EdgeO;
        vtx_types[v] = VtxType::VtxO;
        vtx_types[g.neighbor(v, e_ind)] = VtxType::VtxO;
    }
}

void Solver::break_cycle(long v, EdgeType t) {
    long cid = cycle_ind[v];
    long i0 = cycle_pos[v];
    assert(cid >= 0 && cid < cycles.size());
    assert(i0 >= 0 && i0 < cycles[cid].v.size());
    unsigned long nc = cycles[cid].e_ind_f.size();
    for (unsigned long j = 0; j < nc; ++j) {
        long i = (i0 + j) % nc;
        if (vtx_types[cycles[cid].v[i]] == VtxType::VtxT) {
            if (t == EdgeType::EdgeO) {
                t = EdgeType::EdgeI;
            } else {
                t = EdgeType::EdgeO;
            }
        }
        edge_types[g.eid(cycles[cid].v[i], cycles[cid].e_ind_f[i])] = t;
    }
    // Clear vertex types
    for (unsigned long i = 0; i < cycles[cid].v.size(); ++i) {
        vtx_types[cycles[cid].v[i]] = VtxType::VtxO;
    }
}

void Solver::augment_hit_A(long v, long e_ind) {
    vector<pair<long, long> > P = unwind_path(v, e_ind);
    xor_path(P);
    recompute_packing();
}

void Solver::augment_hit_spoke(long v, long e_ind) {
    vector<pair<long, long> > P = unwind_path(v, e_ind);
    augment_hit_spoke(P);
}

void Solver::augment_hit_spoke(vector<pair<long, long> > &P) {
    pair<long, long> p = P.back();
    long u = g.neighbor(p.first, p.second);
    long sid = path_ind[u];
    long i = path_pos[u];
    while (i < spokes[sid].e_ind_f.size()) {
        edge_types[g.eid(spokes[sid].v[i], spokes[sid].e_ind_f[i])] = EdgeType::EdgeO;
        vtx_types[spokes[sid].v[i]] = VtxType::VtxO;
        i++;
    }
    vtx_types[spokes[sid].v[i]] = VtxType::VtxO;
    break_cycle(spokes[sid].v[i], EdgeType::EdgeO);
    xor_path(P);
    recompute_packing();
}

void Solver::augment_hit_cycle(long v, long e_ind, value_t b) {
    long u = g.neighbor(v, e_ind);
    if (cycles[cycle_ind[u]].val_f[cycle_pos[u]] != b) {
        break_cycle(u, EdgeType::EdgeO);
    } else {
        break_cycle(u, EdgeType::EdgeI);
    }
    xor_path(v, e_ind);
    recompute_packing();
}

vector<pair<long, long> > Solver::unwind_path(long v, long e_ind) {
    vector<pair<long, long> > ret;
    while(v >= 0) {
        ret.push_back({v, e_ind});
        if (escape_prev_v[v] >= 0) {
            // First edge of an integral path, need to jump back 2 steps
            ret.push_back({prev_v[v], prev_e[v]});
            e_ind = escape_prev_e[v];
            v = escape_prev_v[v];
        } else {
            e_ind = prev_e[v];
            v = prev_v[v];
        }
    }
    reverse(ret.begin(), ret.end());
    return ret;
};

vector<pair<long, long> > Solver::unwind_path(long v) {
    if (escape_prev_v[v] >= 0) {
        vector<pair<long, long> > P = unwind_path(escape_prev_v[v], escape_prev_e[v]);
        P.push_back({prev_v[v], prev_e[v]});
        return P;
    } else {
        return unwind_path(prev_v[v], prev_e[v]);
    }
}

value_t Solver::value_one_step(long v, long p, value_t a) {
    long eid = g.eid(v, p);
    long u = g.neighbor(v, p);
    if (edge_types[eid] == EdgeType::EdgeO) {
        return g.cnst(v, p)->forcedLeft(a);
    } else if (edge_types[eid] == EdgeType::EdgeS) {
        long pid = path_ind[v];
        assert(pid == path_ind[u]);
        assert(path_pos[u] + 1 == path_pos[v]);
        return spokes[pid].val_f[path_pos[u]];
    } else {
        assert(edge_types[eid] == EdgeType::EdgeI); // Should not be invoked on H edges
        long pid = path_ind[v];
        assert(pid == path_ind[u]);
        if (path_pos[u] < path_pos[v]) {
            assert(path_pos[u] + 1 == path_pos[v]);
            return integral_paths[pid].val_f[path_pos[u]];
        } else {
            assert(path_pos[u] == path_pos[v] + 1);
            return integral_paths[pid].val_r[path_pos[u]];
        }
    }

}

bool Solver::juggle_pair(vector<pair<long, long> > &Pv, vector<pair<long, long> > &Pu) {
    long markPv = ++marker_cnt;
    long stop_id = 0;
    vector<value_t> Pv_values(Pv.size() + 1);
    Pv_values[0] = initial_vals[Pv[0].first];
    for (long i = 0; i < Pv.size(); ++i) {
        Pv_values[i + 1] = value_one_step(Pv[i].first, Pv[i].second, Pv_values[i]);
        marker[Pv[i].first] = markPv;
        marker_data[Pv[i].first] = i;
        if (stop_id == i && Pv[i] == Pu[i]) {
            stop_id++;
        }
    }
    while (Pu.size() > stop_id) {
        pair<long, long> p = Pu.back();
        long eid = g.eid(p.first, p.second);
        if (edge_types[eid] == EdgeType::EdgeO) {
            Pu.pop_back();
            Pv.push_back(g.rev_edge(p.first, p.second));
            Pv_values.push_back(value_one_step(Pv.back().first, Pv.back().second, Pv_values.back()));
            marker[Pv.back().first] = markPv;
            marker_data[Pv.back().first] = Pv.size() - 1;
        } else if (edge_types[eid] == EdgeType::EdgeS) {
            long sid = path_ind[p.first];
            long i = path_pos[p.first];
            while (i < spokes[sid].v.size() && marker[spokes[sid].v[i]] != markPv) {
                i++;
            }
            if (i == spokes[sid].v.size()) {
                return true; // Pv is actually an augmenting path ending at spoke sid
            }
            Pu.clear();
            for (auto it : Pv) {
                if (it.first == spokes[sid].v[i]) {
                    break;
                }
                Pu.push_back(it);
            }
            for (long j = i; j >= path_pos[p.first]; --j) {
                Pu.push_back({spokes[sid].v[j], spokes[sid].e_ind_r[j - 1]});
            }
            return false;
        } else if (edge_types[eid] == EdgeType::EdgeI) {
            long pid = path_ind[p.first];
            long x = g.neighbor(p.first, p.second);
            long i = path_pos[p.first];
            long delta = i - path_pos[x]; // Direction in which to travel
            while(i >= 0 && i < integral_paths[pid].v.size() && marker[integral_paths[pid].v[i]] != markPv) {
                i += delta;
            }
            if (i >= 0 && i < integral_paths[pid].v.size()) {
                long y = integral_paths[pid].v[i];
                assert(marker_data[y] > 0 && Pv[marker_data[y]].first == y);
                pair<long, long> q = Pv[marker_data[y] - 1];
                long epyid = g.eid(q.first, q.second);
                if (edge_types[epyid] != EdgeType::EdgeO ||
                    //g.cnst(q.first, q.second)->forcedLeft(vals[q.first]) !=
                    Pv_values[marker_data[y]] !=
                        (delta > 0 ? integral_paths[pid].val_r[i] : integral_paths[pid].val_f[i])) {
                    // Shortcut!
                    Pu.clear();
                    for (auto it : Pv) {
                        if (it.first == y) {
                            break;
                        }
                        Pu.push_back(it);
                    }
                    for (long j = i; j != path_pos[x]; j -= delta) {
                        if (delta > 0) {
                            Pu.push_back({integral_paths[pid].v[j], integral_paths[pid].e_ind_r[j - 1]});
                        } else {
                            Pu.push_back({integral_paths[pid].v[j], integral_paths[pid].e_ind_f[j]});
                        }
                    }
                    return false;
                }
            }
            // Just back up
            while(Pu.size() > stop_id && edge_types[g.eid(p.first, p.second)] == EdgeType::EdgeI) {
                Pu.pop_back();
                Pv.push_back(g.rev_edge(p.first, p.second));
                Pv_values.push_back(value_one_step(Pv.back().first, Pv.back().second, Pv_values.back()));
                marker[Pv.back().first] = markPv;
                marker_data[Pv.back().first] = Pv.size() - 1;
                assert(!Pu.empty());
                p = Pu.back();
            }
        } else {
            assert(false); // should not happen
        }
    }
    return false;
}

void Solver::augment_pair(long v, long e_ind) {
#ifdef DEBUG_PRINTS
    fprintf(stderr, "---- Augment pair(%ld, %ld)\n", v, e_ind);
#endif
    debug_print_chosen_edges();

    long u = g.neighbor(v, e_ind);
    assert(vals[u] >= 0);
    vector<pair<long, long> > Pv = unwind_path(v, e_ind), Pu = unwind_path(u);
    debug_print_path(Pv);
    debug_print_path(Pu);
    if (juggle_pair(Pu, Pv)) {
        // Pu becomes an augmenting path hitting a spoke
        augment_hit_spoke(Pu);
        return;
    }
    debug_print_chosen_edges();
    debug_print_path(Pv);
    debug_print_path(Pu);
    if (juggle_pair(Pv, Pu)) {
        // Pv becomes an augmenting path hitting a spoke
        augment_hit_spoke(Pv);
        return;
    }
    debug_print_chosen_edges();
    debug_print_path(Pv);
    debug_print_path(Pu);
    unsigned long i = 0;
    while (i < Pu.size() && i < Pv.size() && Pu[i] == Pv[i]) {
        xor_edge(Pu[i].first, Pu[i].second);
        i++;
    }
    while (i < Pv.size() && edge_types[g.eid(Pv[i].first, Pv[i].second)] != EdgeType::EdgeO) {
        i++;
    }
    marker_cnt++;
    for (long j = i; j < Pv.size(); ++j) {
        marker[g.eid(Pv[j].first, Pv[j].second)] = marker_cnt;
    }
    vector<long> to_set;
    while (i < Pv.size()) {
        long eid = g.eid(Pv[i].first, Pv[i].second);
        if (edge_types[eid] == EdgeType::EdgeO) {
            marker[eid] = 0;
            to_set.push_back(eid);
            i++;
            continue;
        }
        long x = Pv[i].first, back_ind = Pv[i].second;
        long hit_eid = -1;
        while (initial_vals[x] < 0) {
            long j = 0;
            while (j < g.degree(x)) {
                if (j != back_ind && edge_types[g.eid(x, j)] != EdgeType::EdgeO) {
                    break;
                }
                j++;
            }
            assert(j < g.degree(x));
            if (marker[g.eid(x, j)] == marker_cnt) {
                hit_eid = g.eid(x, j);
                break;
            }
            back_ind = g.rev_edge(x, j).second;
            x = g.neighbor(x, j);
        }
        if (hit_eid >= 0) {
            while(i < Pv.size()) {
                xor_edge(Pv[i].first, Pv[i].second);
                marker[g.eid(Pv[i].first, Pv[i].second)] = 0;
                if (g.eid(Pv[i].first, Pv[i].second) == hit_eid) {
                    i++;
                    break;
                }
                i++;
                assert(i < Pv.size());
            }
        } else {
            while(i < Pv.size() && edge_types[g.eid(Pv[i].first, Pv[i].second)] != EdgeType::EdgeO) {
                marker[g.eid(Pv[i].first, Pv[i].second)] = 0;
                i++;
            }
        }
    }
    for (auto eid : to_set) {
        edge_types[eid] = EdgeType::EdgeI;
    }
    debug_print_chosen_edges();
    recompute_packing();
}

DFSResult Solver::recompute_dfs(long v, long eid, bool seen_T) {
    marker[v] = marker_cnt;
    int deg_cnt = 0;
    long back_ind = -1;
    long e_inds[2];
    for (long j = 0; j < g.degree(v); ++j) {
        long eidj = g.eid(v, j);
        assert(eidj >= 0 && eidj < edge_types.size());
        if (eidj != eid && edge_types[eidj] != EdgeType::EdgeO) {
            assert(deg_cnt < 2);
            e_inds[deg_cnt++] = j;
        } else if (eidj == eid) {
            back_ind = j;
        }
    }
    assert(eid < 0 || back_ind >= 0);
    if (deg_cnt == 0) {
        if (eid < 0) {
            // Not used in packing at all
            return DFSResult::NotUsed;
        } else if (seen_T) {
            assert(initial_vals[v] >= 0);
            vtx_types[v] = VtxType::VtxS;
            edge_types[eid] = EdgeType::EdgeS;
            spokes.push_back(ForcingPath());
            spokes.back().v.push_back(v);
            spokes.back().e_ind_f.push_back(back_ind);
            return DFSResult::SpokeBackward;
        } else {
            assert(initial_vals[v] >= 0);
            vtx_types[v] = VtxType::VtxI;
            edge_types[eid] = EdgeType::EdgeI;
            integral_paths.push_back(ForcingPath());
            integral_paths.back().v.push_back(v);
            integral_paths.back().e_ind_f.push_back(back_ind);
            return DFSResult::Path;
        }
    } else {
        bool isT = ((deg_cnt == 2) || (eid >= 0 && deg_cnt == 1 && initial_vals[v] >= 0));
        DFSResult final_res = DFSResult::Path;
        for (int it = 0; it < deg_cnt; ++it) {
            long w = g.neighbor(v, e_inds[it]);
            if (marker[w] == marker_cnt) {
                if (seen_T) {
                    // Closed a cycle!
                    cycles.push_back(ForcingPath());
                    cycles.back().v.push_back(w);
                    cycles.back().e_ind_f.push_back(g.rev_edge(v, e_inds[it]).second);
                    cycles.back().v.push_back(v);
                    cycles.back().e_ind_r.push_back(e_inds[it]);
                    edge_types[g.eid(v, e_inds[it])] = EdgeType::EdgeH;
                    final_res = DFSResult::Cycle;
                } // else this is the last outgoing edge from the first T vertex in a wheel -> do nothing
            } else {
                DFSResult res = recompute_dfs(w, g.eid(v, e_inds[it]), seen_T || isT);
                if (res == DFSResult::Path) {
                    integral_paths.back().v.push_back(v);
                    integral_paths.back().e_ind_r.push_back(e_inds[it]);
                } else if (res == DFSResult::SpokeBackward) {
                    spokes.back().v.push_back(v);
                    spokes.back().e_ind_r.push_back(e_inds[it]);
                } else if (res == DFSResult::SpokeForward) {
                    spokes.back().v.push_back(v);
                    spokes.back().e_ind_f.push_back(e_inds[it]);
                } else {
                    assert(res == DFSResult::Cycle);
                    assert(!cycles.empty());
                    cycles.back().v.push_back(v);
                    cycles.back().e_ind_r.push_back(e_inds[it]);
                }
                final_res = res;
            }
        }
        if (!isT) {
            if (final_res == DFSResult::Path) {
                if (back_ind >= 0) {
                    integral_paths.back().e_ind_f.push_back(back_ind);
                    edge_types[eid] = EdgeType::EdgeI;
                }
                vtx_types[v] = VtxType::VtxI;
                return DFSResult::Path;
            } else if (final_res == DFSResult::SpokeBackward) {
                spokes.back().e_ind_f.push_back(back_ind);
                assert(back_ind >= 0);
                edge_types[eid] = EdgeType::EdgeS;
                vtx_types[v] = VtxType::VtxS;
                return DFSResult::SpokeBackward;
            } else if (final_res == DFSResult::SpokeForward) {
                if (back_ind >= 0) {
                    spokes.back().e_ind_r.push_back(back_ind);
                    edge_types[eid] = EdgeType::EdgeS;
                } else {
                    // The end of a wheel
                    reverse(spokes.back().v.begin(), spokes.back().v.end());
                    reverse(spokes.back().e_ind_f.begin(), spokes.back().e_ind_f.end());
                    reverse(spokes.back().e_ind_r.begin(), spokes.back().e_ind_r.end());
                }
                vtx_types[v] = VtxType::VtxS;
                return DFSResult::SpokeForward;
            } else {
                assert(final_res == DFSResult::Cycle);
                assert(back_ind >= 0);
                assert(!cycles.empty());
                cycles.back().e_ind_f.push_back(back_ind);
                edge_types[eid] = EdgeType::EdgeH;
                vtx_types[v] = VtxType::VtxH;
                return DFSResult::Cycle;
            }
        } else {
            vtx_types[v] = VtxType::VtxT;
            if (seen_T) {
                if (initial_vals[v] >= 0) {
                    /* Zero-length spoke */
                    assert(deg_cnt == 1);
                    spokes.push_back(ForcingPath());
                    spokes.back().v.push_back(v);
                }
                assert(!cycles.empty());
                cycles.back().e_ind_f.push_back(back_ind);
                edge_types[eid] = EdgeType::EdgeH;
                return DFSResult::Cycle;
            } else {
                // Starting last spoke!
                spokes.push_back(ForcingPath());
                spokes.back().v.push_back(v);
                if (eid >= 0) {
                    spokes.back().e_ind_r.push_back(back_ind);
                    edge_types[eid] = EdgeType::EdgeS;
                }
                return DFSResult::SpokeForward;
            }
        }
    }
}

void Solver::recompute_packing() {
    ++marker_cnt;
    integral_paths.clear();
    spokes.clear();
    cycles.clear();
    for (auto pid : inst.init_vals) {
        if (marker[pid.first] != marker_cnt) {
            recompute_dfs(pid.first, -1, false);
        }
    }
    for (long pid = 0; pid < integral_paths.size(); ++pid) {
        value_t a = initial_vals[integral_paths[pid].v[0]];
        integral_paths[pid].val_f.push_back(a);
        for (long i = 0; i < integral_paths[pid].v.size(); ++i) {
            long v = integral_paths[pid].v[i];
            path_ind[v] = pid;
            path_pos[v] = i;
            if (i < integral_paths[pid].e_ind_f.size()) {
                Constraint *c = g.cnst(v, integral_paths[pid].e_ind_f[i]);
                assert(c->isForcedLeft(a));
                a = c->forcedLeft(a);
                integral_paths[pid].val_f.push_back(a);
            }
        }
        value_t b = initial_vals[integral_paths[pid].v.back()];
        integral_paths[pid].val_r.push_back(b);
        for (long i = integral_paths[pid].e_ind_r.size(); i > 0; --i) {
            Constraint *c = g.cnst(integral_paths[pid].v[i], integral_paths[pid].e_ind_r[i-1]);
            assert(c->isForcedLeft(b));
            b = c->forcedLeft(b);
            integral_paths[pid].val_r.push_back(b);
        }
        reverse(integral_paths[pid].val_r.begin(), integral_paths[pid].val_r.end());
    }
    for (long sid = 0; sid < spokes.size(); ++sid) {
        value_t a = initial_vals[spokes[sid].v[0]];
        spokes[sid].val_f.push_back(a);
        for (long i = 0; i < spokes[sid].v.size(); ++i) {
            long v = spokes[sid].v[i];
            path_ind[v] = sid;
            path_pos[v] = i;
            if (i < spokes[sid].e_ind_f.size()) {
                Constraint *c = g.cnst(v, spokes[sid].e_ind_f[i]);
                assert(c->isForcedLeft(a));
                a = c->forcedLeft(a);
                spokes[sid].val_f.push_back(a);
            }
        }
    }
    for (long cid = 0; cid < cycles.size(); ++cid) {
        assert(cycles[cid].v[0] == cycles[cid].v.back());
        assert(path_ind[cycles[cid].v[0]] >= 0 && path_ind[cycles[cid].v[0]] < spokes.size());
        value_t a = spokes[path_ind[cycles[cid].v[0]]].val_f.back();
        for (long i = 0; i < cycles[cid].e_ind_f.size(); ++i) {
            cycles[cid].val_f.push_back(a);
            long u = cycles[cid].v[i + 1];
            if (vtx_types[u] == VtxType::VtxT) {
                a = spokes[path_ind[u]].val_f.back();
            } else {
                assert(vtx_types[u] == VtxType::VtxH);
                Constraint *c = g.cnst(cycles[cid].v[i], cycles[cid].e_ind_f[i]);
                assert(c->isForcedLeft(a));
                a = c->forcedLeft(a);
            }
        }
        cycles[cid].val_f.push_back(a);
        a = spokes[path_ind[cycles[cid].v.back()]].val_f.back();
        for (long i = cycles[cid].e_ind_f.size(); i > 0; --i) {
            cycles[cid].val_r.push_back(a);
            long u = cycles[cid].v[i - 1];
            cycle_ind[u] = cid;
            cycle_pos[u] = i - 1;
            if (vtx_types[u] == VtxType::VtxT) {
                a = spokes[path_ind[u]].val_f.back();
            } else {
                assert(vtx_types[u] == VtxType::VtxH);
                Constraint *c = g.cnst(cycles[cid].v[i], cycles[cid].e_ind_r[i - 1]);
                assert(c->isForcedLeft(a));
                a = c->forcedLeft(a);
            }
        }
        cycles[cid].val_r.push_back(a);
        reverse(cycles[cid].val_r.begin(), cycles[cid].val_r.end());
    }
}

CSPSolution Solver::packing2hitting() {
    CSPSolution s = CSPSolution();
    for (unsigned long i = 0; i < integral_paths.size(); ++i) {
        if (vis_f[i] == vis_r[i]) {
            s.ones.push_back(integral_paths[i].v[vis_f[i]]);
        } else {
            s.halves.push_back(integral_paths[i].v[vis_f[i]]);
            s.halves.push_back(integral_paths[i].v[vis_r[i]]);
        }
    }
    for (unsigned long i = 0; i < spokes.size(); ++i) {
        s.halves.push_back(spokes[i].v[vis_spokes[i]]);
    }
    // Yoichi Iwata's trick: instead of the lower bound
    //   s.ones.size() + (s.halves.size() + 1) / 2
    // we can observe that every cycle + spokes needs (#spokes + 1) / 2 vertices.
    s.lower_bound = integral_paths.size() + (spokes.size() + cycles.size()) / 2;
    return s;
}

void Solver::farthest() {
    long s = g.add_extra_vertex();
    vtx_types[s] = VtxType::VtxA;
    EqualConstraint cnst;
    long eid = m - 1;
    edge_types[eid] = EdgeType::EdgeO;
    for (unsigned long pid = 0; pid < integral_paths.size(); ++pid) {
        bool augmented = false;
        while (!augmented && vis_f[pid] < vis_r[pid]) {
            long i = vis_f[pid] + 1;
            initial_vals[s] = integral_paths[pid].val_f[i];
            g.add_edge(s, integral_paths[pid].v[i], eid, &cnst);
            augmented = augment(s);
            assert(augmented || vis_f[pid] >= i);
            g.remove_edge_by_index(s, 0);
        }
        augmented = false;
        while (!augmented && vis_f[pid] < vis_r[pid]) {
            long i = vis_r[pid] - 1;
            initial_vals[s] = integral_paths[pid].val_r[i];
            g.add_edge(s, integral_paths[pid].v[i], eid, &cnst);
            augmented = augment(s);
            assert(augmented || vis_r[pid] <= i);
            g.remove_edge_by_index(s, 0);
        }
    }
    for (unsigned long sid = 0; sid < spokes.size(); ++sid) {
        bool augmented = false;
        while (!augmented && vis_spokes[sid] < spokes[sid].v.size() - 1) {
            long i = vis_spokes[sid] + 1;
            initial_vals[s] = spokes[sid].val_f[i];
            g.add_edge(s, spokes[sid].v[i], eid, &cnst);
            augmented = augment(s);
            assert(augmented || vis_spokes[sid] >= i);
            g.remove_edge_by_index(s, 0);
        }
    }
    g.pop_extra_vertex();
}

CSPSolution Solver::solve() {
    n = g.num_vertices() + 1; // One extra for farthest()
    m = g.num_edges() + 1;
    prev_v = vector<long>(n, -1);
    prev_e = vector<long>(n, -1);
    escape_prev_v = vector<long>(n, -1);
    escape_prev_e = vector<long>(n, -1);
    marker = vector<long>((n > m) ? n : m, 0);
    marker_data = vector<long>((n > m) ? n : m, 0);
    vtx_types = vector<VtxType>(n, VtxType::VtxO);
    edge_types = vector<EdgeType>(m, EdgeType::EdgeO);
    path_pos = vector<long>(n, -1);
    path_ind = vector<long>(n, -1);
    cycle_pos = vector<long>(n, -1);
    cycle_ind = vector<long>(n, -1);
    initial_vals = vector<value_t>(n, -1);
    vals = vector<value_t>(n, -1);
    for (auto p : inst.init_vals) {
        vtx_types[p.first] = VtxType::VtxA;
        initial_vals[p.first] = p.second;
    }
    while(augment());
    farthest();
    return packing2hitting();
}

void Solver::debug_print_chosen_edges() {
#ifdef DEBUG_PRINTS
    fprintf(stderr, "====== Chosen Edges =======\n");
    for (long v = 0; v < n; ++v) {
        bool printed = false;
        for (unsigned long p = 0; p < g.degree(v); ++p) {
            if (edge_types[g.eid(v, p)] != EdgeType::EdgeO) {
                if (!printed) {
                    fprintf(stderr, "%4ld: ", v);
                    printed = true;
                }
                fprintf(stderr, " (p=%3lu, eid=%3ld, u=%3ld, type=%d)", p, g.eid(v, p), g.neighbor(v, p), edge_types[g.eid(v, p)]);
            }
        }
        if (printed) {
            fprintf(stderr, "\n");
        }
    }
#endif
}

void Solver::debug_print_path(vector<pair<long, long> > P) {
#ifdef DEBUG_PRINTS
    fprintf(stderr, "Path(%2lu): ", P.size());
    for (auto p : P) {
        fprintf(stderr, "%3ld[p=%3ld, eid=%3lu] ", p.first, p.second, g.eid(p.first, p.second));
    }
    if (!P.empty()) {
        fprintf(stderr, "%3ld\n", g.neighbor(P.back().first, P.back().second));
    } else {
        fprintf(stderr, "empty\n");
    }
#endif
}
