#include <cstdio>
#include <vector>

using std::vector;

vector<vector<int> > g;
int n;
vector<int> val;

bool dfs(int v, int s) {
    val[v] = s;
    for (auto w : g[v]) {
        if (val[w] == 0) {
            if (dfs(w, 3 - s)) {
                return true;
            }
        } else if (val[w] == s) {
            return true;
        }
    }
    return false;
}

int main(int argc, char *argv[]) {
    FILE *f;
    int opt = -1;

    if (argc > 3) {
        // OPT provided
        f = fopen(argv[3], "r");
        fscanf(f, "%d", &opt);
        fclose(f);
    }

    f = fopen(argv[1], "r");
    int m, a, b;
    fscanf(f, "%d%d", &n, &m);
    g.resize(n);
    while(m--) {
        fscanf(f, "%d%d", &a, &b);
        g[a].push_back(b);
        g[b].push_back(a);
    }
    fclose(f);
    val.resize(n, 0);

    int sol_size;
    f = fopen(argv[2], "r");
    fscanf(f, "%d", &sol_size);
    for (int i = 0; i < sol_size; ++i) {
        fscanf(f, "%d", &a);
        if (a < 0 || a > n) {
            printf("wrong vertex id: %d", a);
            return -1;
        }
        if (val[a]) {
            printf("vertex twice in solution: %d", a);
        }
        val[a] = -1;
    }
    fclose(f);

    for (int v = 0; v < n; ++v) {
        if (val[v] == 0 && dfs(v, 1)) {
            printf("graph not bipartite");
            return -1;
        }
    }
    printf("solution size = %4d", sol_size);
    if (opt >= 0){
        printf (", opt = %4d ", opt);
        if (opt < sol_size)
            return 1;
        else if (opt == sol_size)
            return 0;
        else
            return 2;
    } else {
        printf(", opt unknown");
        return 0;
    }
}