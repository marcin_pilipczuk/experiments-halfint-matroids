#ifndef CSPSOLVER_MWC_TOOLS_H
#define CSPSOLVER_MWC_TOOLS_H

#include <cstdio>
#include <vector>
#include "mwc.h"

using std::vector;
using std::pair;

enum FFState {
    FFReachable, FFNotReachable, FFCut
};

vector<FFState> ford_fulkerson(vector<vector<long> > &g, vector<long> &X, vector<long> &Y) {
    unsigned long n = g.size();
    vector<long> flow(n, -1), prev(n, -1), vis(n, 0), jump(n, -1);
    for (auto x : X) {
        flow[x] = -2;
    }
    for (auto y : Y) {
        vis[y] = -1;
    }
    long cnt = 0;
    bool found;
    do{
        cnt++;
        found = false;
        vector<long> stack = X;
        for (auto x : X) {
            vis[x] = cnt;
        }
        while(!found && !stack.empty()) {
            long v = stack.back();
            stack.pop_back();
            for (auto u : g[v]) {
                if (flow[u] == v || flow[v] == u) {
                    continue;
                }
                if (vis[u] == -1) {
                    while(prev[v] >= 0) {
                        if (flow[prev[v]] == v) {
                            if (jump[v] >= 0) {
                                flow[prev[v]] = jump[v];
                                v = jump[v];
                            } else {
                                flow[prev[v]] = -1;
                                v = prev[v];
                            }
                        } else {
                            flow[v] = prev[v];
                            v = prev[v];
                        }
                    }
                    found = true;
                    break;
                }
                if (vis[u] == cnt) {
                    continue;
                }
                if (flow[u] != -1) {
                    long w = flow[u], w2 = u;
                    bool fix = (w >= 0 && vis[w] < cnt);
                    while (w >= 0 && vis[w] < cnt) {
                        vis[w] = cnt;
                        jump[w] = -1;
                        prev[w] = w2;
                        stack.push_back(w);
                        w2 = w;
                        w = flow[w];
                    }
                    if (fix) {
                        jump[flow[u]] = v;
                    }
                } else {
                    prev[u] = v;
                    vis[u] = cnt;
                    jump[u] = -1;
                    stack.push_back(u);
                }
            }
        }
    } while(found);
    vector<FFState> ret(n, FFNotReachable);
    for (long v = 0; v < n; ++v) {
        if (vis[v] == cnt) {
            ret[v] = FFReachable;
            for (auto u : g[v]) {
                if (ret[u] != FFReachable) {
                    ret[u] = FFCut;
                }
            }
        }
    }
    return ret;
}

class MWCFFSolver : public MWCSolver {
protected:
    vector<vector<long> > g;

    void remove_vertex(vector<vector<long> > &curr_g, long v) {
        for (auto u : curr_g[v]) {
            for (unsigned long i = 0; i < curr_g[u].size(); ++i) {
                if (curr_g[u][i] == v) {
                    curr_g[u][i] = curr_g[u].back();
                    curr_g[u].pop_back();
                    break;
                }
            }
        }
        g[v].clear();
    }

public:
    void initialize() {
        read_input();
        g.resize(n);
        for (auto e : edges) {
            g[e.first].push_back(e.second);
            g[e.second].push_back(e.first);
        }
    }
};

class MWCMinCutAppxSolver : public MWCFFSolver {
public:

    void solve() {
        vector<long> X = terminals, Y;
        while (X.size() > 1) {
            long t = X.back();
            X.pop_back();
            Y.push_back(t);
            vector<FFState> cut = ford_fulkerson(g, X, Y);
            for (long v = 0; v < n; ++v) {
                if (cut[v] == FFCut) {
                    best_solution.push_back(v);
                    remove_vertex(g, v);
                }
            }
            Y.pop_back();
        }
    }
};

class MWCImpSepSolver : public MWCFFSolver {
protected:
    vector<bool> is_terminal;

    void initialize() {
        MWCFFSolver::initialize();
        is_terminal = vector<bool>(n, false);
        for (auto t : terminals) {
            is_terminal[t] = true;
        }
    }

    void merge_onto_t(vector<vector<long> > &curr_g, vector<FFState> &cut, long t) {
        curr_g[t].clear();
        for (long v = 0; v < n; ++v) {
            if (cut[v] == FFNotReachable && v != t) {
                curr_g[v].clear();
            } else if (cut[v]== FFCut) {
                unsigned long j = 0;
                for (unsigned long i = 0; i < curr_g[v].size(); ++i) {
                    if (cut[curr_g[v][i]] != FFNotReachable) {
                        curr_g[v][j++] = curr_g[v][i];
                    }
                }
                while (j < curr_g[v].size()) {
                    curr_g[v].pop_back();
                }
                curr_g[v].push_back(t);
                curr_g[t].push_back(v);
            }
        }
    }

    void branching(vector<vector<long> > &curr_g) {
        unsigned long sol_size = current_solution.size();
        if (solution_found && current_solution.size() >= best_solution.size()) {
            return;
        }
        vector<long> X = terminals, Y;
        if (X.size() <= 1) {
            try_update_solution();
            return;
        }
        long t = X.back();
        Y.push_back(t);
        X.pop_back();
        vector<FFState> cut = ford_fulkerson(curr_g, X, Y);
        merge_onto_t(curr_g, cut, t);

        if (solution_found && current_solution.size() + curr_g[t].size() >= best_solution.size()) {
            return;
        }

        long best = -1, best_score = -1;
        for (auto v : curr_g[t]) {
            bool ok = true;
            long score = 0;
            for (auto u : curr_g[v]) {
                if (u != t && is_terminal[u]) {
                    current_solution.push_back(v);
                    ok = false;
                    remove_vertex(curr_g, v);
                    break;
                } else if (cut[u] == FFReachable) {
                    score++;
                }
            }
            if (ok && score > best_score) {
                best = v;
                best_score = score;
            }
        }

        if (best >= 0) {
            vector<vector<long> > copy = curr_g;

            current_solution.push_back(best);
            remove_vertex(curr_g, best);
            branching(curr_g);
            current_solution.pop_back();

            for (auto u : copy[best]) {
                if (cut[u] == FFReachable) {
                    copy[u].push_back(t);
                    copy[t].push_back(u);
                }
                remove_vertex(copy, best);
            }
            branching(copy);
        } else {
            terminals.pop_back();
            branching(curr_g);
            terminals.push_back(t);
        }

        while (current_solution.size() > sol_size) {
            current_solution.pop_back();
        }
    }

public:
    void solve() {
        branching(g);
    }
};
#endif //CSPSOLVER_MWC_TOOLS_H
